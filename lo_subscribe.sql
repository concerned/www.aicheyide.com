/*
Navicat MySQL Data Transfer

Source Server         : loan
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : loan

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-10-17 18:15:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lo_subscribe
-- ----------------------------
DROP TABLE IF EXISTS `lo_subscribe`;
CREATE TABLE `lo_subscribe` (
  `subscribe_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `user_phone` int(11) NOT NULL COMMENT '预约用户id',
  `goods_id` tinyint(4) NOT NULL COMMENT '预约商品id',
  `subscribe_ctiy_name` char(120) NOT NULL COMMENT '预约门店',
  `subscribe_name` char(30) NOT NULL COMMENT '预约人姓名',
  `subscribe_sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '预约人性别 0为女 1为男',
  `subscribe_time` char(20) NOT NULL COMMENT '预约到店时间',
  `subscribe_city` char(20) NOT NULL COMMENT '预约城市',
  PRIMARY KEY (`subscribe_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_subscribe
-- ----------------------------
