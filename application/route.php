<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

Route::rule('type','public/home/type/index');
Route::rule('problem','public/home/problem/index');
Route::rule('subscribe','public/home/subscribe/index');
Route::rule('follow','public/home/follow/index');
Route::rule('details/:goods_id','public/home/details/index','get');
Route::rule('dies','public/home/index/dies');
// Route::rule('problem','public/home/problem/index');
// Route::rule('index','public/home/index/index');