<?php
function pr($var) {
	if (config('app_debug')) {
		$template = PHP_SAPI !== 'cli' ? '<pre>%s</pre>' : "\n%s\n";
		printf($template, print_r($var, true));
	}
}

