<?php
namespace app\common\model;
 
use think\Db;
/** 
* 
* 问题数据库操作类 
*/  
class Problem extends App{
	/**  
	* 问题模块的插入操作
	* @access public 
	* @param mixed $data 需要插入的数据
	* @return int
	*/  
	public static function insert(array $data){
		// pr($data);die;
		$res = Db::name('problem')->insert($data);
		if($res) return $res;
	}
    

    /**  
	* 问题模块的查询操作加搜索分页
	* @access public 
	* @param mixed $where 搜索条件
	* @param mixed $options url参数
	* @return array $res查询数据结果
	*/  
	public static function select($where,$options){
		// pr($data);die;
		$res = Db::name('problem')->where($where)->order('problem_id desc')->paginate(5,false,$options);;
	
		if($res) return $res;
	}

	/**  
	* 问题模块的修改时默认值查询
	* @access public 
	* @param mixed $id 需要修改数据的id
	* @return array $res 默认值数据
	*/  
	public static function edit($id){
		// pr($data);die;
		$res = Db::name('problem')->find($id);
		// pr($res);die;
		if($res) return $res;
	}

	/**  
	* 问题模块的修改
	* @access public 
	* @param mixed $id 需要修改的数据
	* @return int $res 修改成功还是失败
	*/  
	public static function modify($data){
		// pr($data);die;
		$res = Db::name('problem')->update($data);
		// pr($res);die;
		if($res) return $res;
	}

	/**  
	* 问题模块的删除
	* @access public 
	* @param mixed $id 需要删除数据的id
	* @return int $res 删除成功还是失败
	*/  
	public static function del($problem_id){
		// pr($data);die;
		$res = Db::name('problem')->delete($problem_id);
		// pr($res);die;
		if($res) return $res;
	}
}