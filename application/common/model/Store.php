<?php
namespace app\common\model;
 
use think\Db;
/** 
* 
* 门店信息数据库操作类 
*/  
class Store extends App{
		/**  
	* 门店信息城市的插入操作
	* @access public 
	* @param mixed $data 需要插入的数据
	* @return int 插入的id
	*/  
	public static function add(array $data){
		$res = Db::name('store')->insert($data);
		//getLastInsID
		// return $res;
		if(!$res)return false;
		if($uid = Db::name('store')->getLastInsID()){
			return $uid;
		}
	}
	/**
	* 门店信息城市的查询操作
	* @access public 
	* @param mixed $data 需要插入的数据
	* @return array 是否存在
	*/  
	public static function find($where){
		$res = Db::name('store')->where($where)->find();
		if(!$res)return false;
		return $res;
	}

	/**
	* 门店信息城市的插入操作
	* @access public
	* @return array 查询结果
	*/  
	public static function select(){
			$res = Db::name('store')->order('store_uid asc')->select();
			if(!$res)return false;
			foreach($res as $k=>$v){
				if($v['store_level'] == 1){
				$res[$k]['path']=$v['store_id'].','.$v['store_uid'];
				$res[$k]['paths']=0;
			}else if($v['store_level'] == 2){
				$res[$k]['path']=$v['store_uid'].','.$v['store_id'];
				$res[$k]['paths']=1;
			}
		}

		return $res;
	}

		/**
	* 门店详细地址的修改操作
	* @access public 
	* @param mixed $data 需要修改的数据
	* @return int 是否修改成功
	*/  

	public static function edit($data){
		$res = Db::name('store')->update($data);
		if(!$res)return false;
		return $res;
	}
	

			/**
	* 门店详细地址的删除操作
	* @access public 
	* @param mixed $store_id 需要删除数据的id
	* @return int 是否删除成功
	*/  

	public static function del($store_id){
		$res = Db::name('store')->delete($store_id);
		if(!$res)return false;
		return $res;
	}

		/**
	* 门店详细地址的显示操作
	* @access public 
	* @param mixed $data 需要修改的数据与id
	* @return int 是否修改成功
	*/  
	public static function state($data){
		$res = Db::name('store')->update($data);
		// pr($res);
		if(!$res)return 0;
		return $res;
	}
			
}