<?php
namespace app\common\model;
use think\Db;
class Follow extends App{
			/**  
			* 用户关注的展示操作
			* @access public 
			* @return array 查询的结果
			*/  
	public static function select(){
		$res = Db::name('follow')->alias('a')
		->join('goods_details b','a.goods_id = b.goods_id')
		->field('a.*,b.goods_name,b.goods_model,b.goods_describe,b.goods_car')
		->order('follow_id desc')
		->select();
		// pr($res);
		return $res;
	}
}