<?php
namespace app\common\model;
use think\Db;
/*
网站配置模块操作

*/
class Website extends App{
	/**  
	* 网站配置的查询操作
	* @access public 
	* @return array 查询的数据
	*/  
	public static function index(){
		$res = Db::name('website_config')->find();
		if(!$res) return false;
		return $res;
	}

		/**  
	* 网站配置的修改log操作
	* @access public 
	* @return int 是否修改成功
	*/  
	public static function picUpdate($data){
		$res = Db::name('website_config')->where(array('website_id'=>array('eq',1)))->update($data);
		if(!$res) return false;
		return $res;
	}
	public static function keywordsSel(){
		
	}

	
}