<?php
namespace app\common\model;
 
use think\Db;
class Navigation extends App{
	/**  
	* 导航模块的添加
	* @access public 
	* @param mixed $data 需要添加的数据
	* @return int $res 添加成功还是失败
	*/  
	public static function add($data){
		$res = Db::name('navigation')->insert($data);
		return $res;
	}
	/**  
	* 导航模块的查询
	* @access public 
	* @return array $res 查询的结果集
	*/  
	public static function cat(){
		$res = Db::name('navigation')->select();
		return $res;
	}
	/**  
	* 导航模块的单条查询
	* @access public 
	* @param mixed $id 需要查询的id
	* @return array $res 查询结果
	*/  
		public static function find($navigation_sort){
		$res = Db::name('navigation')->where(array('navigation_sort'=>array('eq',$navigation_sort)))->find();
		return $res;
	}

	public static function ajaxUpdate($data,$where){
		$res = Db::name('navigation')->where($where)->update($data);
		return $res;
	}
	public static function maxs(){
		$res = Db::name('navigation')->max('navigation_sort');
		return $res;
	}
}