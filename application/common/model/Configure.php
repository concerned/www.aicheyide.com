<?php

namespace app\common\model;
 
use think\Db;

class Configure extends App{

		public static function adds($id){
			$res = Db::name('goods_configure')->alias('a')
			->join('goods_details b','a.goods_id=b.goods_id')
			->field('a.*,b.goods_id,b.goods_name')
			->where('a.goods_id',$id)->find();
			$res1 = Db::name('goods_details')->field('goods_id,goods_name')->where(array('goods_id'=>array('eq',$id)))->find();
			$res2 = array($res,$res1);
			return $res2;
		}
        

		public static function inserts($data){
			$res = Db::name('goods_configure')->insert($data);
			return $res;
		}

		public static function updates($data){
			$res = Db::name('goods_configure')->update($data);
			return $res;
		}


 }