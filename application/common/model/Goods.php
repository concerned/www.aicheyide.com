<?php
namespace app\common\model;
 
use think\Db;

class Goods extends App{
	public static function select($num){
		$res = Db::name('goods_details')->where('goods_state',$num)->select();
		return $res;
	}

/*	public static function selects(){
		$res = Db::name('goods_details')->where('type_state',0)->select();
		return $res;
	}*/
	 public static function insert(array $data){
		
	 $res = Db::name('goods_details')->insert($data);
	 if($res){
	 	$goodsId = Db::name('goods_details')->getLastInsID();
	 }
	 	return $goodsId;
	 }

	  public static function dateilss($options,$where){
	  	// pr()
		$res = Db::name('goods_details')->where($where)->order('goods_id desc')->paginate(5,false,$options); 	
	 	return $res;
	 }

	 public static function edit($id){
	 	 $Map=[
		    'goods_state'=>'0',
		    'goods_id'=>$id
		];
	 	$res = Db::name('goods_details')->where($Map)->find();
	 	return $res;
	 }

	  public static function updates($data){
	 	
	 	$res = Db::name('goods_details')->update($data);
	 	// pr($res);die;
	 	return $res;
	 }
	   public static function state($goodsid,$state){
	 	$data['goods_status'] = $state;
	 	// return $state;
	 	$res = Db::name('goods_details')->where(array('goods_id'=>array('eq',$goodsid)))->update($data);
	 	// $res;die;
	 	return $res;
	 }
	 public static function ajaxDel($goodsid){
		$data['goods_state'] = 1;
		$arr['car_state'] = 1;
		$res = Db::name('goods_details')->where(array('goods_id'=>array('eq',$goodsid)))->update($data);
		if($res){
			$res1 = Db::name('goods_configure')->where(array('goods_id'=>array('eq',$goodsid)))->update($arr);
		return $res1;
		}
		
		}

		public static function ajaxShow($goodsid,$goods_show){
			$data['goods_show'] = $goods_show;
			$res = Db::name('goods_details')->where(array('goods_id'=>array('eq',$goodsid)))->update($data);
	 	// $res;die;
	 	return $res;
		}

		public static function recycle($options,$wheres){
			$res = Db::name('goods_details')->where($wheres)->order('goods_id desc')->paginate(5,false,$options);
			return $res;
		}

		public static function ajaxRecycle($goods_id,$type_id){
			$data['goods_state'] = 0;
			$arr['car_state']=0;
			$where['type_id']=$type_id;
			$where['type_state'] =1;
			// $res = Db::name('goods_types')->where($where)->find();
			// return $res;
			if(!Db::name('goods_types')->where($where)->find()){
				$res = Db::name('goods_details')->where(array('goods_id'=>array('eq',$goods_id)))->update($data);
				if(Db::name('goods_configure')->where(array('goods_id'=>array('eq',$goods_id)))->select()){
						if($res){
							$res1 = Db::name('goods_configure')->where(array('goods_id'=>array('eq',$goods_id)))->update($arr);
							}
							return $res1;
				}
				return $res;
			}else {
				return false;
			}
			
		}
}
