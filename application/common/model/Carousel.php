<?php
namespace app\common\model;
 
use think\Db;

class Carousel extends App{
			/**  
	* 轮播信息的插入操作
	* @access public 
	* @param mixed $data 需要插入的数据
	* @return int 插入是否成功
	*/  
		public static function add($data){
				$res = Db::name('carousel')->insert($data);
				if(!$res) return false;
				return $res;
		}


			/**  
	* 轮播信息的查询操作
	* @access public
	* @return array 查询的数据
	*/  
		public static function select(){
				$res = Db::name('carousel')->select();
				if(!$res) return false;
				return $res;
		}

		/**  
	* 轮播的修改操作
	* @access public
	* @param mixed $data 需要修改的数据
	* @return int 是否修改成功
	*/  
		public static function updates($data){
				$res = Db::name('carousel')->update($data);
				if(!$res) return false;
				return $res;
		}

		/**  
	* 轮播的单条数据查询操作
	* @access public
	* @param mixed $id 需要查询数据的id
	* @return array 查询到的数据
	*/  
		public static function find($id){
				$res = Db::name('carousel')->where(array('carousel_id'=>array('eq',$id)))->find();
				if(!$res) return false;
				return $res;
		}


		/**  
	* 轮播的单条数据删除操作
	* @access public
	* @param mixed $id 需要删除数据的id
	* @return int 是否删除成功
	*/  
		public static function del($id){
				$res = Db::name('carousel')->where(array('carousel_id'=>array('eq',$id)))->delete();
				if(!$res) return false;
				return $res;
		}
}