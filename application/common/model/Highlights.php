<?php
namespace app\common\model;
 
use think\Db;

class Highlights extends App{
	public static function index_value(){
		
		$res = Db::name('goods_highlights')->select();
		return $res;
	}
	public static function values($goods_id){
		$res = Db::name('goods_details')->field('goods_name,goods_id')->where('goods_id',$goods_id)->find();
		return $res;
	}
	public static function adds(array $data){
		$res = Db::name('goods_highlights')->insert($data);
		return $res;
	}

	public static function find($where){
		
		$res = Db::name('goods_highlights')->where($where)->find();
		return $res;
	}


	public static function updates($data){
		
		$res = Db::name('goods_highlights')->update($data);
		return $res;
	}
		public static function ajaxDel($car_id){
		
		$res = Db::name('goods_highlights')->where(array('car_id'=>array('eq',$car_id)))->delete();
		return $res;
	}

	public static function cat($goods_id){
		// pr($goods_id);
		$res = Db::name('goods_highlights')->where(array('goods_id'=>array('eq',$goods_id)))->select();
		return $res;
	}
}