<?php
namespace app\common\model;
 
use think\Db;
class Type extends App{
	//品牌查询
	public static function selects($options,$where){
		$res = Db::name('goods_types')->where($where)->order('type_id desc')->paginate(5,false,$options);
		return $res;
	}
	public static function select($type_state){
		$res = Db::name('goods_types')->where('type_state',$type_state)->select();
		return $res;
	}
	public static function cat(){
		$res = Db::name('goods_types')->select();
		return $res;
	}
	//品牌数据添加
	public static function insert(array $data){
		
	$res = Db::name('goods_types')->insert($data);
		return $res;
	}
	//品牌名称ajax查询
	public static function ajaxSelect($typeName){
	
	$res = Db::name('goods_types')->where(array('type_name'=>array('eq',$typeName)))->find();
		return $res;
	}
	//品牌名称ajax修改
	public static function ajaxUpdate($typeName){
		// return $typeName;
		$res = Db::name('goods_types')->where(array('type_name'=>array('eq',$typeName)))->find();
		return $res;
	}
	//品牌ajax删除
	public static function ajaxDel($type_id){
		$data['type_state'] = 1;
		$arr['goods_state'] = 1;
		$array['car_state'] = 1;
		
		$res = Db::name('goods_types')->where(array('type_id'=>array('eq',$type_id)))->update($data);
		if($res){
		$goods_id = Db::name('goods_details')->field('goods_id')->where(array('type_id'=>array('eq',$type_id)))->select();
		// $str = implode(',',$goods_id);
		if(!empty($goods_id)){
			$str = '';
		foreach ($goods_id as $key => $value) {
			$str .= implode(',',$value).',';
		}
		$goods_id  = rtrim($str,',');
		$where['goods_id'] = array('in',$goods_id);
		// return $goods_id;
			$res1 = Db::name('goods_details')->where(array('type_id'=>array('eq',$type_id)))->update($arr);
			$goods_configure = Db::name('goods_configure')->where($where)->select();
			if($goods_configure){
				$res2 = Db::name('goods_configure')->where($where)->update($array);
				return $res2;
			}
			return $res1;
		}
		return $res;
		}
		
		
		}
	//品牌修改模板默认数据
	public static function edit($type_id){
			$res = Db::name('goods_types')->where(array('type_id'=>array('eq',$type_id)))->find();
			return $res;
	}

	//品牌分类修改
	public static function updates($data){
		$res = Db::name('goods_types')->Update($data);
		return $res;
	}

	public static function ajaxRecycle($type_id){

		$data['type_state'] = 0;

		
		$res = Db::name('goods_types')->where(array('type_id'=>array('eq',$type_id)))->update($data);
		// if($res){
		// $goods_id = Db::name('goods_details')->field('goods_id')->where(array('type_id'=>array('eq',$type_id)))->select();
		// // return $res;
		// // pr($goods_id);die;
		// if(!empty($goods_id)){
		// 	$str = '';
		// foreach ($goods_id as $key => $value) {
		// 	$str .= implode(',',$value).',';
		// }
		// $goods_id  = rtrim($str,',');
		// $where['goods_id'] = array('in',$goods_id);
		// // return $goods_id;
		// 	$res1 = Db::name('goods_details')->where(array('type_id'=>array('eq',$type_id)))->update($arr);
		// 	$goods_configure = Db::name('goods_configure')->where($where)->select();
		// 	if($goods_configure){
		// 		$res2 = Db::name('goods_configure')->where($where)->update($array);
		// 			return $res2;
		// 	}
		
		//     return $res1;
		// }
		// // $str = implode(',',$goods_id);
		// 
		// }
			return $res;
	}
}