<?php
namespace app\common\model;
use think\Db;

class User extends App
{
	
		/**  
	* 用户模块的查询
	* @access public 
	* @return array $res 添加成功还是失败
	*/ 

	public static function select(){
		$res  = Db::name('login_info')->select();
		return $res;
	}

		/**  
	* 超级管理员的查询
	* @access public 
	* @return array $res 添加成功还是失败
	*/ 

	public static function adminSelect(){
		$res  = Db::name('admin_users')->select();
		return $res;
	}  
}