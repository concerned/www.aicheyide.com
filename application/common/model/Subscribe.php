<?php
namespace app\common\model;
use think\Db;
class Subscribe extends App{

	/**  
	* 预约信息的展示操作
	* @access public 
	* @return array 查询的结果
	*/  
	public static function select(){
		$res = Db::name('subscribe')->alias('a')
		->join('goods_details b','a.goods_id = b.goods_id')
		->field('a.*,b.goods_name')
		->order('subscribe_state desc')
		->select();
		// pr($res);
		return $res;
	}

	/**  
	* 预约信息的车辆详情的展示操作
	* @access public 
	* @param mixed $id 需要查询的数据
	* @return array 查询的结果
	*/  
	public static function find($id){
		$res = Db::name('goods_details')->find($id);
		// pr($res);
		return $res;
	}

	/**  
	* 预约信息的处理修改操作
	* @access public 
	* @param mixed $data 需要修改的数据
	* @return int 是否修改成功
	*/  
	public static function updates($data){
		$res = Db::name('subscribe')->update($data);
		// pr($res);
		return $res;
	}


	/**  
	* 预约信息的处理详情查询
	* @access public 
	* @param mixed $where 需要查询的数据条件
	* @return array 查询的数据
	*/  
	public static function edit($where){
		$res = Db::name('subscribe')->field('subscribe_result')->where($where)->select();
		// pr($res);
		return $res;
	}
}