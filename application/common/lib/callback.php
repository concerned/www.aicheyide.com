<?php
namespace app\common\lib;

class callback{
    protected $ts; ##控制器对象
    protected $m;##当前模块
    protected $c;##当前控制名
    protected $a;##当前方法名
    protected $ca; ##控制器::方法名
    public $appBeforeAction = NULL;
    public $appAfterAction  = NULL;
    
    public function __construct($controller){
        $this->ts = $controller ;
        $this->m  = $this->ts->params['module'];
        $this->c  = $this->ts->params['controller'];
        $this->a  = $this->ts->params['action'];
        $this->ca = $this->c.'::'.$this->a;
        
        $appBeforeAction = 'appBefore'.\think\Loader::parseName($this->m,1);
        $appAfterAction  = 'appAfter'.\think\Loader::parseName($this->m,1);
        if(is_callable([$this,$appBeforeAction]))  $this->appBeforeAction = $appBeforeAction;
        if(is_callable([$this,$appAfterAction])) $this->appAfterAction = $appAfterAction;
    }
    ##每次访问URL方法之前（不管任何模块）都会执行
    ##在这里面可以写入自己的通用逻辑代码，而又不需要动核心App控制器代码
    ##这些方法里面的代码写法和控制器里面一样，只是用$this->ts 表示 控制器的$this
    public function appBeforeEach(){}
    ##每次访问URL方法后、渲染页面之前（不管任何模块，这个时候所有需要assign到页面的数据都已经准备好）都会执行
    public function appAfterEach(){}
    ##每次访问URL方法之前（仅限Home模块方法）都会执行
    public function appBeforeHome(){
        ##前台每个页面都需要家长的css 和js 
        $this->ts->assign->addJs('jquery-3.2.1.min.js');
        $this->ts->assign->addJs('/files/layui/layui.js');
        $this->ts->assign->addCss('/files/layui/css/layui.css');
        $this->ts->assign->addCss('global.css');
        $this->ts->assign->addCss('animate.css');
        
        ##可以类似于这样 单独指定不同控制器不同方法执行的代码
        switch($this->ca){
            case 'Article::show':
                ##文字列表页
                break;
            case 'Product::show':
                ##产品列表页
                break;
            case 'Product::view':
                ##产品详情页
                break;
        }
        
        if($this->ca =='Index::index'){
            ##首页页面
            $this->ts->assign->is_index = true ;
            $this->ts->assign->addCss('index.css');
            
        }else{
            ##内页页面（非首页）
            $this->ts->assign->addCss('insider.css');
            $this->ts->assign->addCss('change.css');
        }
    }
    ##每次访问URL方法后、渲染页面之前（仅限Home模块方法，这个时候所有需要assign到页面的数据都已经准备好）都会执行
    public function appAfterHome(){
        
    }
    ##每次访问URL方法之前（仅限Run模块方法）都会执行
    public function appBeforeRun(){
        $this->ts->assign->addJs('jquery-3.2.1.min.js');
        $this->ts->assign->addJs('/files/layui/layui.js');
        $this->ts->assign->addJs('admin/global.js');
        $this->ts->assign->addJs('/files/artDialog/js/jquery.artDialog.min.js');
		$this->ts->assign->addJs('/files/artDialog/js/artDialog.plugins.min.js');
		$this->ts->assign->addCss('/files/artDialog/css/simple.css');
        
        $this->ts->assign->addCss('/files/layui/css/layui.css');
        $this->ts->assign->addCss('admin/global.css');
        $this->ts->assign->addCss('admin/animate.css');
        $this->ts->assign->addCss('/files/awesome-4.7.0/css/font-awesome.min.css');
        
    }
    ##每次访问URL方法后、渲染页面之前（仅限Run模块方法，这个时候所有需要assign到页面的数据都已经准备好）都会执行
    public function appAfterRun(){}
    ##以后扩展模块，每个模块都支持添加这样的2个方法用来表示 执行URL指定方法前和后的 动作
    
}