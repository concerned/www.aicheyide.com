<?php
namespace app\common\controller;
use think\Controller;
use think\Config;
use think\Loader;
use think\Request;
use think\Db;
class App extends Controller{
	public $file,$files,$params,$args,$absroot,$root,$assign; 
	//哪些控制器不用验证
	public $notLogin = array('login');

	public function _initialize(){
		 
		//获取URL参数
		$passedArgs     = input('') ;
		//获取当前模块、控制器、方法
		$request = Request::instance();
		//当前访问参数
		$this->params = array(
			'module' =>$request->module(),
			'controller' =>$request->controller(),
			'action' =>$request->action(),
			'module' =>$request->module(),
			'args' =>$passedArgs
		);
		//当前URL参数
		$this->args   =    $passedArgs ; 
		//IIS编码修正
	    if(strpos($_SERVER['SERVER_SOFTWARE'],'Microsoft-IIS')!==false){            
			foreach($this->passedArgs as &$passedArg){
				$passedArg    = mb_convert_encoding($passedArg,'UTF-8','GBK');
			}
		} 
		switch (strtolower($this->params['module'])) {
			//前台
			case 'home':
				
			 	// $this->addCss('/validator/css/bootstrapValidator');
			 	$this->addCss('/bootstrap/css/bootstrap.min');
				// $this->addCss('/home/css/buttons');
			 	// $this->addCss('/font-awesome');
       			 // $this->addJs('/home/js/fang/common');
				$this->addCss('/home/layui-2.0/css/layui');
				$this->addCss('/home/css/iconfont');
			 	
			 	$this->addCss('/home/css/fang/modal.min');
			 	$this->addCss('/home/css/global');
			 	$this->addCss('/riqi/css/bootstrap-datetimepicker.min');
			 

			 	// $this->addCss('/home/css/fang/nav-hover');
			 	// $this->addCss('/global');
			 	$this->addJs('/jquery-1.11.1.min');
			 	$this->addJs('/home/js/global');
			 	$this->addJs('/css3-mediaqueries'); 
			 	$this->addJs('/bootstrap/js/bootstrap.min');

			 	// $this->assign('controllerActive',strtolower($this->params['controller']));
			 	

			 	// $this->addJs('/validator/js/bootstrapValidator');
			 	// $this->addJs('/html5shiv.min'); 
			 	// $this->addJs('/pic');
			 	// 聊天插件
			 	// $this->addCss('/home/css/style');
     // 			$this->addCss('/home/css/htmleaf-demo');
     			//弹出层
     		 
				break;
			//后台
		 	case 'run':
			//判断是否登陆，（除login）	
			// dump(strtolower($this->params['controller']));die;
			if(!session('?RUN_USER')&& !in_array(strtolower($this->params['controller']),$this->notLogin)){
				// echo 1;die;
				$this->redirect('run/Login/login');
			} 
			//公用css
			$this->addJs('/jquery-1.11.1.min');
			$this->addCss('/run/css/font');
			$this->addCss('/run/css/xadmin');
			$this->addCss('/run/css/swiper.min');
			$this->addCss('/run/lib/layui/css/layui');
			$this->addCss('/font-awesome');
			$this->addCss('/global');
			$this->addCss('/bootstrap/css/bootstrap.min');
			$this->addJs('/bootstrap/js/bootstrap.min');
			$this->addCss('/run/css/fileinput');
			$this->addJs('/run/js/fileinput');
			$this->addJs('/run/js/fileinput_locale_zh');
			//公用js
			
			$this->addJs('/run/js/swiper.jquery.min');
			$this->addJs('/run/lib/layui/layui');
			$this->addJs('/html5shiv.min');
			// $this->addJs('/run/js/echarts-for-x-admin');
			$this->addJs('/run/js/xadmin');
			$this->addJs('/pic');
			//日期插件my97
		 	$this->addJs('/My97DatePicker/WdatePicker');
     		//百度编辑器
     		/*$this->addJs('/ueditor/ueditor.config');
     		$this->addJs('/ueditor/ueditor.all.min');
     		$this->addJs('/ueditor/lang/zh-cn/zh-cn');
		 	//bootstrap表单验证*/
 			//汉字转拼音
			$this->addJs('/run/js/pinyin_dict_notone');
			$this->addJs('/run/js/pinyin_dict_withtone');
			$this->addJs('/run/js/pinyinUtil');
			$this->addJs('/run/js/simple-input-method');
			// $this->addCss('/run/css/simple-input-method');
 			
		 	
				break;
		}
		 //当前相对根域名
		 $this->root     =__ROOT__;
		 
		 if(substr($this->root,0,8) =='/wwwroot') $this->root = substr($this->root,8);
		 
		 
		 //当前绝对根域名 $this->absroot = 项目目录/public
		 $this->absroot= $request->domain().$request->root();
		  
		 
		 $absrootArray = explode('/',$this->absroot);

		 if(end($absrootArray) == 'index.php'){
		     array_pop($absrootArray);
		     $this->absroot = implode('/',$absrootArray);
		 }
		 
		 
		$this->assign('static',$this->absroot.'/static/');
		$this->assign('run',$this->absroot.'/static/run/');
		$this->assign('home',$this->absroot.'/static/home/');
		$this->assign('root',$this->root) ;
 		$this->assign('absroot',$this->absroot.'/') ;	
 		$this->assign('rootpath',ROOT_PATH);
		//判断当前是否通过手机端访问
		// $this->isMobile =  $this->assign->isMobile =   $this->request->isMobile();
		        
		//当前服务器时间  
		// $this->now      =  $this->assign->now =  time();  
		##重写assign方法，统一管理，让变量传出都通过我们自己的Assign对象传出

	}
/*	protected function assign($name,$value='') {
	    $this->assign->$name = $value ;
	    return true ;
	}  */

	function addJs($file){
	    if(substr($file,0,1) != '/' && !preg_match('/^http:\/\//' ,$file)){
	       
	        $file ='/js/'.$file;
	         
	    }
	    if(!preg_match('/^http:\/\//' ,$file)){
	        $file = 'static'.$file;
	       
	    }        
	    if(strtolower(substr($file,-3)) !='.js'){
	        $file .= '.js'; 
	    } 
	    
	    $this->file['js'][]= $file ;
	    
	    
	}

	function addCss($files){
	    if(substr($files,0,1) != '/' && !preg_match('/^http:\/\//' ,$files)){
	        $files ='/css/'.$files;
	    }
	    if(!preg_match('/^http:\/\//' ,$files)){
	        $files = 'static'.$files;
	    }        
	    if(strtolower(substr($files,-3)) !='.css'){
	        $files .= '.css'; 
	    }
	    $this->files['css'][] = $files ;        
	}
	//重写fetch方法
	protected function fetch($templateFile='',$charset='',$contentType='',$content='',$prefix='') {
	    
	    $this->assign('params',$this->params);
	    $this->assign('js',$this->file['js']);
	    $this->assign('css',$this->files['css']);
	    return call_user_func_array(array('parent',__FUNCTION__),func_get_args());
	}
	
	##表单自动验证
	protected function auto_proof($data){
	    $rule = [
	        'title'=> 'require',
	        'ex_title'   => 'require'
	        ];
	    $msg = [
	        'title.require' => '名称必须',
	        'ex_title.require'=> '副标题必须'
	        ];
	    $validate = new \think\Validate($rule,$msg);
	    $result   = $validate->batch()->check($data);
	    if(!$result){
	        return $validate->getError();
	        }
	    
	}
	public function upload($file_input_name){
	    $Config=array(
	        'size'=>3145728,
	        'ext'=>array('jpg', 'gif', 'png', 'jpeg')
	    );
	    $file=request()->file($file_input_name);
	    foreach($file as $k=>$v){
	        $info=$v->validate($Config)->move(ROOT_PATH . 'public' . DS . 'uploads');
	        if($info){
	            $y[]='public' . DS . 'uploads\\'.$info->getSaveName();
	            pr($y);die;
	            $this->thumb_save_name[]=$info->getFilename();
	            
	        }else{
	            $error['photo'][]=$v->getError();
	            $this->assign('error',$error);
	        }
	    }
	    return $y;
	}
	public function be_thumb($img_path,$width=150,$height=150,$thumb_save='./public/uploads/thumbs/',$type_code=1){
	    //文件夹不存在就创建
	    if(!file_exists($thumb_save))  mkdir($thumb_save);
	    
	    if(!$this->thumb_save_name){
	        
	        foreach($img_path as $k=>$v){
	            $image= \think\Image::open($v);
	            $this->img_width=$image->width();
	            $this->img_height=$image->height();
	            $v_arr=explode('\\',$v);
	            $name=$v_arr[count($v_arr)-1];
	            $thumb_save_path=$thumb_save.$name;
	            
	            $image->thumb($width,$height,$type_code)->save($thumb_save_path);
	            $thumb_save_path_array[$k]=$thumb_save_path;
	        }
	        
	    }else{
	    
	    foreach($this->thumb_save_name as $k=>$v){
	        //pr($img_path[$k]);exit;
	        $image= \think\Image::open($img_path[$k]);
	        
	        $thumb_save_path=$thumb_save.$v;
	        
	        $image->thumb($width,$height,$type_code)->save($thumb_save_path);
	        $thumb_save_path_array[$k]=$thumb_save_path;
	    }
	}
	    $this->thumb_save_name='';
	    return $thumb_save_path_array;
	}
	
	
	##页面输出前Assign出所有变量
/*	protected function _assignVars(){
	    ##Assign出全局变量        
	    $this->assign->args     = $this->args ;
	    $this->assign->params   = $this->params ;
	    $this->assign->this     = $GLOBALS['controller'];
	    
	    if(!$this->assign->_VarsReturned){
	        foreach($this->assign as $_var=>$_value){
	            parent::assign($_var , $_value);
	        }
	        $this->assign->_VarsReturned = true ;
	    }
	    return true ;
	}*/
}