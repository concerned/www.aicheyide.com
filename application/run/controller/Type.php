<?php
namespace app\run\controller;
use think\Request;
use think\Db;
use app\common\model\Type as typeModel;
class Type extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
	}

	public function index(){
		$options    = ['query' =>[]];

		  $this->assign('type_name', '');
		  $where['type_state'] = 0;
		if(request()->isGet()){
				$type_name = input('get.type_name');

				// pr($goods_name);
				 if ($type_name) {
				 	$where['type_name']=array('like',"%{$type_name}%");
           			
           			 $this->assign('type_name', $type_name);
					 $options['query']['type_name'] = $type_name;
        		}
		}
	
		$this->addCss('/run/css/page');
		$page = input('get.page');
		 $num = typeModel::select(0);
		 $num = count($num);
		if($res=typeModel::selects($options,$where)){
	
			$num = count($res);
			$this->assign('res',$res);
			$this->assign('num',$num);
			// pr($res);die;
			return $this->fetch();
		}else{
			return $this->error('没有品牌管理','Index/index');
		}
		return $this->fetch();
	}
	//添加ajax
	public function ajaxSelect(){
		if(request()->isPost()){
			$typeName = input('post.type_name');
			
			if(typeModel::ajaxSelect($typeName)){
				echo 1;
			}else{
				echo 0;
			}
		}
	}
	//修改ajax
	public function ajaxUpdate(){
		if(request()->isPost()){
			$typeName = input('post.type_name');
			$type_id = input('post.type_id');
			// pr($typename);
			$res = typeModel::ajaxUpdate($typeName);
			// pr($res);die;
			if($res['type_id'] == $type_id || !$res){
				echo 0;
			}else{
				echo 1;
			}
		}
	}

	//分类添加
	public function add(Request $request){

		$this->assign('error','');
		if(request()->isPost()){

			$time = date('Y-m-d',time());
			$newTime = str_replace('-','',$time);
			$data = array();
			$data['type_name'] = trim(input('type_name'));
			$data['type_pinyin'] = trim(input('type_pinyin'));
			$file = $request->file('file');
			$info = $file->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
			if($info){
	
			$data['type_pic'] =  'static/uploads/'.$newTime.'/'.$info->getFilename();
			$data['type_pic'] = addslashes($data['type_pic']);
			if(typeModel::insert($data)){
				 $this->redirect('run/Type/index');
			}else{
				unlink(DS.'public'.DS.$data['type_pic']);
				$this->assign('error','品牌上传失败,请重新上传');
			}

			}else{
			// 上传失败获取错误信息
				$this->assign('error',$file->getError());
			
			}
			
		}
		return $this->fetch();

	}

	public function edit(){
		$this->assign('error','');
		if(request()->isGet()){
			$type_id = input('type_id');
	
			if($res = typeModel::edit($type_id)){
				$this->assign('res',$res);
				return $this->fetch();
			}
						
		}
		
	}

	public function update(Request $request){
		if(request()->isPost()){
			$time = date('Y-m-d',time());
			$newTime = str_replace('-','',$time);
			$data['type_id'] = input('type_id');
			$data['type_name'] = input('type_name');
			$type_pic = input('type_pic');
			if($request->file('file')){
				$file = $request->file('file');
				$info = $file->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
				if($info){	
					$data['type_pic'] =  'static/uploads/'.$newTime.'/'.$info->getFilename();
					$data['type_pic'] = addslashes($data['type_pic']);
					
					if(typeModel::updates($data)){
						@unlink(ROOT_PATH.DS.'public'.DS.$type_pic);
						$this->redirect('run/Type/index');
					}else{
						$this->redirect('run/Type/index',array('type_id'=>$data['type_id']));
					}
				}else{
						$this->redirect('run/Type/index',array('type_id'=>$data['type_id']));
				}
			

			}else{
					if(typeModel::updates($data)){
						$this->redirect('run/Type/index');
					}else{
						$this->redirect('run/Type/index',array('type_id'=>$data['type_id']));
					}
		}
	}
	
}


	public function ajaxDel()
	{
		if(request()->isPost()){
			$type_id = input('post.type_id');
			
			
			if(typeModel::ajaxDel($type_id)){
				echo 1;
			}else{
				echo 0;
			}
		}
	}

	public function recycle(){
		$this->assign('type_id','');
		if(input('id')){
			$type_id = input('id');
			$this->assign('type_id',$type_id);
		}
		
		$options    = ['query' =>[]];
					 $where['type_state'] = 1;
		 			$this->assign('type_name', '');
					if(request()->isGet()){
					$type_name = input('get.type_name');

				// pr($goods_name);
				 if ($type_name) {
				 	$where['type_name']=array('like',"%{$type_name}%");
           	
           			 $this->assign('type_name', $type_name);
					 $options['query']['type_name'] = $type_name;
        		}
		}
			$res = typeModel::selects($options,$where);
			
			$num = typeModel::select(1);
		
				$num = count($num);
			if(	$res){

				$this->assign('res',$res);
			
				$this->assign('num',$num);
				return $this->fetch();
			}
	}

	public function ajaxRecycle(){
		if(request()->isPost()){
				$type_id = input('post.type_id');
				// pr($type_id);die;
				$res = typeModel::ajaxRecycle($type_id);
				// pr($res);die;
				if($res){
					echo 1;
				}else{
					echo 0;
				}

			}
	}

}