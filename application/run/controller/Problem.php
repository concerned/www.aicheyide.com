<?php
namespace app\run\controller;
use think\Request;
use app\common\model\Problem as problemModel;
use think\View;
class Problem extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
	}


	public function add(){
		$this->assign('error','');
		if(request()->isPost()){
			$data = input('post.');
			$res = problemModel::insert($data);
			if($res){
				$this->redirect('run/problem/index');
			}else{
				$this->assign('error','添加失败');
			}
		}
			return $this->fetch();
	}
	public function index(){
		$options    = ['query' =>[]];
		 $where = [];
		  $this->assign('problem_name', '');
		if(request()->isGet()){
				$problem_name = input('get.problem_name');

				// pr($goods_name);
				 if ($problem_name) {
				 	$where['problem_name']=array('like',"%{$problem_name}%");
           	
           			 $this->assign('problem_name', $problem_name);
					 $options['query']['problem_name'] = $problem_name;
        		}
		}
	

		    if(!$res = problemModel::select($where,$options)) {$this->assign('error','没有数据,请添加数据');return $this->fetch('add');}
		    $this->assign('res',$res);
			return $this->fetch();
	}

	public function update(){
		$this->assign('error','');
		if(request()->isGet()){
			$id = input('id');
			// pr($id);
			if(!$res = problemModel::edit($id)) $this->redirect('run/problem/index');
			// pr($res);die;
			 $this->assign('res',$res);
			return $this->fetch('edit');
		}

		if(request()->isPost()){
			$data = input('post.');
		    if(!problemModel::modify($data))$this->redirect('run/problem/update',array('id' => $data['problem_id']));
		    $this->redirect('run/problem/index');
		}
	}


	public function ajaxDel(){
		if(request()->isPost()){
			$problem_id  =  input('post.id');
			if(!problemModel::del($problem_id)) echo 0;
			echo 1;
		}
	}
}