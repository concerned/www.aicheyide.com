<?php
namespace app\run\controller;
use think\Request;
use app\common\model\Highlights as highlightsModel;
use think\Db;
class Highlights extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
		
	}
	public function index(){
		$res = highlightsModel::index_value();
		$this->assign('res',$res);

		return $this->fetch();
	}
	public function add(Request $request){
		if(request()->isGet()){
			$goods_id = input('goods_id');
			$res = highlightsModel::values($goods_id);

			$this->assign('res',$res);
			 
		}
		 $this->assign('error','');
		 if(request()->isPost()){
		 	$time = date('Y-m-d',time());
		 	$newTime = str_replace('-','',$time);
		 	$data = input('post.');
		 	
		 	$data['goods_name'] = trim(input('goods_name'));
		 	$file = $request->file('car_img');

		 	$info = $file->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
		 	if($info){
		 
		 	$data['car_img'] =  'static/uploads/'.$newTime.'/'.$info->getFilename();
		 	$data['car_img'] = addslashes($data['car_img']);
		 	if(highlightsModel::adds($data)){
		 		 $this->redirect('run/Highlights/index');
		 	}else{
		 		unlink(DS.'public'.DS.$data['car_img']);
		 		$this->assign('error','上传失败,请重新上传');
		 	}

		 	}else{
		 	// 上传失败获取错误信息
		 		$this->assign('error',$file->getError());
		 	
		 	}
		 	
		 }
	 


		return $this->fetch();
	}

	public function edit(){
		$car_id = input('id');
		// pr($car_id);
		$where['car_id'] = $car_id;
		$res = highlightsModel::find($where);
		//$goods = highlightsModel::cat($res['goods_id']);
		// pr($goods);
		if($res){
			$this->assign('res',$res);
			//$this->assign('goods',$goods);
			return $this->fetch();
		}
	}

	public function update(){
		if(request()->isPost()){
			$src = input('post.pic');
			$data['car_id'] = input('post.car_id');
			$data['goods_name'] = input('post.goods_name');
			$data['car_title'] = input('post.car_title');
			$data['car_content'] = input('post.car_content');
			if($pic = request()->file('car_img')){
					$data['car_img'] = addslashes($this->uploads($pic));
					@unlink(ROOT_PATH.DS.'public'.DS.$src);
				}
				// pr($data);
			$res = highlightsModel::updates($data);
			if($res){
				 $this->redirect('run/Highlights/index');
			}
		}
	}

		private function uploads($pic){
    		
    			$info = $pic->validate(['size'=>156780,'ext'=>'jpg,png,gif'])->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
    			if($info){
			      $newTime = str_replace('-','',date('Y-m-d',time())).'/';

			       $picPath =  'static/uploads/'.$newTime.$info->getFilename();
			       return $picPath;
    			}else{
        		
       				 echo $pic->getError();
    		}
		}


		public function  ajaxDel(){
				if(request()->isPost()){
					$car_id = input('post.carid');
					$goods_id = input('post.goods_id');

					$res = highlightsModel::cat($goods_id);
					// pr($res);die;
					if (count($res)>1) {
						$num = highlightsModel::ajaxDel($car_id);
						if($num){
							echo 2;
						}else{
							echo 1;
						}
					}else{
						echo 0;
					}
					
		
				}
		}
}