<?php
namespace app\run\controller;
use app\common\model\Carousel as carouselModel;
class Carousel extends Run{
	public $path;
	public function _initialize(){

		call_user_func(array('parent',__FUNCTION__));
	}
		public function index(){

			$res = carouselModel::select();
			if(!$res){
				//$this->assign('error','没有数据,请添加数据');
				$this->redirect('/run/carousel/add');
			} else{
				$this->assign('res',$res);
				return $this->fetch();
			}
			
		}
		public function add(){
			$this->assign('error','请添加数据');
			if(request()->isPost()){
				$data = input('post.');
				// pr($data);die;
				$file = request()->file('file');
				// pr($file);
				$picPath = $this->uploads($file);
				// pr($picPath);
				$data['carousel_pic'] = $picPath;
				// pr($data);die;
				$res = carouselModel::add($data);
				if(!$res){
					$this->assign('error','轮播添加失败');
					return $this->fetch();
				} 
				$this->redirect('run/carousel/index');
			}
			return $this->fetch();
		}

		public function ajaxState(){
			if(request()->isPost()){
				$data = input('post.');
				// pr($data);die;
				$res = carouselModel::updates($data);
				if(!$res) return 0;
				return 1;
			}
		}

		public function edit(){
			if(request()->isGet()){
				$carousel_id = input('id');
				// $where['carousel_id'] = $carousel_id;
				// pr($where);die;
				$res = carouselModel::find($carousel_id);
				if(!$res) $this->redirect('run/carousel/index');
				// pr($this->absroot);die;
				$this->assign('res',$res);
				//$this->assign('absroot',$this->absroot);
				return $this->fetch();
			}
		}

		public function update(){
			if(request()->isPost()){
				$data = input('post.');
				$res = carouselModel::updates($data);
				if(!$res) return 0;
				return 1;
			}
			
		}

		public function ajaxDel(){
			if(request()->isPost()){
				$id = input('carousel_id');
				$res = carouselModel::del($id);
				if(!$res) return 0;
				return 1;
			}
			
		}
		public function pic(){
			 if(request()->isPost()){
			 // $file = request()->file("pic"); 
			 	$id = input('carousel_id');
			    $file = $_FILES['pic'];
			 // echo json_encode($id);die;
			    if($file["error"] == 1){
			    	$response['isSuccess']="图片上传过大,请重新上传";
				  // echo json_encode($response);

			}else if($file["error"] == 4){
				$response['isSuccess']="没有图片上传";
				  // echo json_encode($response);
			}else if($file['error'] == 3){
				$response['isSuccess']="只有部分文件上传";
			}else{
				$picPath =  $this->uploadz($file);

		   		
		        $response = array();    
		        if($picPath){    
		        	//$this->path = $picPath;
		        	$data['carousel_id'] = $id;
		        	$data['carousel_pic'] = $picPath;
		        	$res = carouselModel::find($id);
		        	carouselModel::updates($data);

		        		@unlink(ROOT_PATH.DS.'public'.DS.$res['website_log']);
		            $response['isSuccess'] = true;    
		            $response['f'] = $picPath;    
		        }else{    
		            $response['isSuccess'] = false;    
		        }    
		                
		        echo json_encode($response);  
			 
			}
			
				}
		}

		private function uploads($pic){
		
			$info = $pic->validate(['size'=>1000000,'ext'=>'jpg,png,gif,jpeg'])->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
			if($info){
		      $newTime = str_replace('-','',date('Y-m-d',time())).'/';

		       $picPath =  'static/uploads/'.$newTime.$info->getFilename();
		       return $picPath;
			}else{
    		
   				 echo $pic->getError();
		}
	}
 
     		private function uploadz($pic){
		 $newTime = str_replace('-','',date('Y-m-d',time())).'/';
		$arr = explode('/',$pic['type']);
		if($arr[1] == 'jpeg'){
			$arr[1] = 'jpg';
		}
		$picname =  'static/uploads/'.$newTime.md5(time().mt_rand(100,1000)).'.'.$arr[1];
		// echo $picname;
		$path = 'static/uploads/'.$newTime;
		if(!file_exists($path)){
			 mkdir ($path);
		}
		$info = move_uploaded_file($pic['tmp_name'],$picname);
		if($info){
			return $picname;
		}else{
			return false;
		}
	}
}