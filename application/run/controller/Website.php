<?php
namespace app\run\controller;
use  think\Model;
use think\Request;
use app\common\model\Website as websiteModel;
class Website extends Run{

	public function index(){
		$res = websiteModel::index();
		if(!$res) return $this->redirect('run/index/index');
		// pr($res);
		$this->assign('res',$res);
		return $this->fetch();
	}

	public function pic(Request $request){
		if(request()->isPost()){
			 // $file = request()->file("pic"); 
			    $file = $_FILES['pic'];
			    if($file["error"] == 1){
			    	$response['isSuccess']="图片上传过大,请重新上传";
				  echo json_encode($response);

			}else if($file["error"] == 4){
				$response['isSuccess']="没有图片上传";
				  echo json_encode($response);
			}else if($file['error'] == 3){
				$response['isSuccess']="只有部分文件上传";
			}else{
				$picPath =  $this->uploads($file);

		   		
		        $response = array();    
		        if($picPath){    
		        	$data['website_log'] = $picPath;
		        	$res = websiteModel::index();
		        	websiteModel::picUpdate($data);

		        		@unlink(ROOT_PATH.DS.'public'.DS.$res['website_log']);
		            $response['isSuccess'] = true;    
		            $response['f'] = $picPath;    
		        }else{    
		            $response['isSuccess'] = false;    
		        }    
		                
		        echo json_encode($response);  
			 
			}
			
				}
		
       
	}

	public function ajaxUpdate(){
		if(request()->isPost()){
			$value = input('newV');
			$key = input('cla');
			$data[$key] = $value;
			// echo $key;
			$res = websiteModel::picUpdate($data);
			if(!$res)return false;
			return 1;
		}
	}

	private function uploads($pic){
		 $newTime = str_replace('-','',date('Y-m-d',time())).'/';
		$arr = explode('/',$pic['type']);
		if($arr[1] == 'jpeg'){
			$arr[1] = 'jpg';
		}
		$picname =  'static/uploads/'.$newTime.md5(time().mt_rand(100,1000)).'.'.$arr[1];
		// echo $picname;
		$path = 'static/uploads/'.$newTime;
		if(!file_exists($path)){
			 mkdir ($path);
		}
		$info = move_uploaded_file($pic['tmp_name'],$picname);
		if($info){
			return $picname;
		}else{
			return false;
		}
	}

}