<?php
namespace app\run\controller;
use app\common\model\Store as storeModel;
use think\Db;
class Store  extends Run
{		
		//门店信息添加
		public function add(){

			return $this->fetch();
		}
		//门店信息列表
		public function index(){
			$res = storeModel::select();
			if(!$res)return false;
			$flag = array();  
  
			foreach($res as $v){  
			    $flag[] = $v['path'];  
			}  
  
			array_multisort($flag, SORT_ASC, $res);  
			// pr($res);  
			// pr($res);
			$this->assign('res',$res);
			return $this->fetch();
		}

		public function ajaxAdd(){
			if(request()->isPost()){
				// 
				
				$data['store_name'] = rtrim(input('store_name'),'市').'市';
				// pr($data['store_name']);die;
				$where['store_name'] =  $data['store_name'];
				$data['store_level'] = 1;
				$data['store_uid'] = 0;
				if($res =storeModel::find($where)){
					// pr($res);die;
					echo $res['store_id'];
				}else{
					$uid = storeModel::add($data);

					if(!$uid)return 0;
					echo $uid;
				}
			}

			}

			public function ajaxInsert(){
			if(request()->isPost()){
				// 
				$data['store_name'] = input('store_name');
				$data['store_company'] = input('store_company');
				$data['store_phone'] = input('store_phone');
				$data['store_level'] = 2;
				$data['store_uid'] = input('uid');
				// pr($data);die;
					$uid = storeModel::add($data);

					if(!$uid)return 0;
					echo $uid;
				}


			}

			public function edit(){
				if(request()->isGet()){
					$where['store_id'] =  input('id');
					$res = storeModel::find($where);
					$wheres['store_id'] = $res['store_uid'];
					$res1 = storeModel::find($wheres);
					if(!$res){
						$this->redirect('run/store/index');
					}else{
						$this->assign('res',$res);
						$this->assign('res1',$res1);
						return $this->fetch();
					} 
				}
				if(request()->isPost()){
					$data['store_id'] =  input('store_id');
					$data['store_name'] =  input('store_name');
					$data['store_company'] =  input('store_company');
					$data['store_phone'] =  input('store_phone');
					
					$res = storeModel::edit($data);
					
					$this->redirect('run/store/index');
				}
			}

			public function ajaxDel(){
					// if(request()->isPost()){
						$store_id = input('store_id');
						// $store_id = 55;
						$res = storeModel::del($store_id);
						if(!$res) return 0;
						return 1;
					// }
			}

			public function ajaxState(){
				// echo 1;
				if(request()->isPost()){
					if(input('store_state')==0){
						$data['store_state'] = 1;
					}else{
						$data['store_state'] = 0;
					}
					$data['store_id'] = input('store_id');
					// pr($data);die;
					$res = storeModel::state($data);
					if(!$res) return 0;
					return 1;

				}
			}
		
}