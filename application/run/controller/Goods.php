<?php
namespace app\run\controller;
use think\Request;
use think\Model;
use think\Db;
use app\common\model\Goods as goodsModel;
use app\common\model\Type as typeModel;
class Goods extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
		
	}
	public function index(){
		$options    = ['query' =>[]];
		 $where['goods_state'] = 0;
		  $this->assign('goods_name', '');
		if(request()->isGet()){
				$goods_name = input('get.goods_name');

				// pr($goods_name);
				 if ($goods_name) {
				 	$where['goods_name']=array('like',"%{$goods_name}%");
           	
           			 $this->assign('goods_name', $goods_name);
					 $options['query']['goods_name'] = $goods_name;
        		}
		}
	
		// pr($where);die;
		
		 
		$arr = input('post.');
		$page = input('get.page');

          	
    	$res = goodsModel::dateilss($options,$where);
    	// pr($res);die;
		$num = goodsModel::select(0);
		$type = typeModel::select(0);
		$num = count($num);
		$this->assign('res',$res);
		$this->assign('type',$type);
		$this->assign('num',$num);
		// pr($res);
		return $this->fetch();
	}

	public function add(){
	 		$this->assign('error','');
		$res = typeModel::select(0);
	    $this->assign('res',$res);
		return $this->fetch();
	}

	public function insert(Request $request){

		if(request()->isPost()){
		
			$data = input('post.');
			if($pic = request()->file('file')){
				$data['goods_pic'] = addslashes($this->uploads($pic));
			}
			$pictures = request()->file('goods_pictures');
			$data['goods_pictures'] = '';
			if($pictures){
				for($i=0;$i<=count($pictures)-1;$i++){
					if((count($pictures)-1)==$i){
							$str = '';
					}else{
						$str = ',';	
					}
					$data['goods_pictures'] .= $this->uploads($pictures[$i]).$str;
					}
				}
				$data['goods_pictures'] = addslashes($data['goods_pictures']);
					// pr(goodsModel::insert($data));die;
				if($goodsId = goodsModel::insert($data)){

					$this->redirect('run/Configure/add',array('goods_id'=>$goodsId));
				}else{
					$this->assign('error','添加商品失败');
					$this->redirect('run/Goods/index');
				}
		
		}
}		
   
		public function edit(){
		if(request()->isGet()){
			$goods = input('id');
			$res = goodsModel::edit($goods);
			$type = typeModel::select(0);
			// pr($res);die;
			$pic = explode(',',$res['goods_pictures']);
			// pr($pic);die;
			$this->assign('res',$res);
			$this->assign('type',$type);
			$this->assign('pic',$pic);
			return $this->fetch();

		}
			
		}

		public function update(Request $request){
			if(request()->isPost()){

				$data = input('post.');
				$src = $data['pic'];
				$dSrc = $data['pictures'];
				unset($data['pic'],$data['pictures']);
		
				// pr($data);die;
				if($pic = request()->file('file')){
					$data['goods_pic'] = addslashes($this->uploads($pic));
				
				}
				if($pictures = request()->file('goods_pictures')){
					// echo 1;die;
					$data['goods_pictures'] = '';
					for($i=0;$i<=count($pictures)-1;$i++){
						if((count($pictures)-1)==$i){
							$str = '';
						}else{
							$str = ',';	
						}
					
						$data['goods_pictures'] .= addslashes($this->uploads($pictures[$i]).$str);
					}
						
				}
				$res = goodsModel::updates($data);
				// pr($res);
				if($res){
					if($pic){
						@unlink(ROOT_PATH.DS.'public'.DS.$src);
					}
					if($pictures){
						$picArr = explode(',',$dSrc);
						foreach($picArr as $v){
							@unlink(ROOT_PATH.'/public/'.$v);
							}
					}
					
					
					$this->redirect('run/goods/index');
					
					
				}else{
					$this->assign('error','修改商品失败');
				}

					
				
				}
		}

		public function ajaxDel(){
			if(request()->isPost()){
			$goodsid = input('post.goods');
			
			// pr($goodsid);die;
			if(goodsModel::ajaxDel($goodsid)){
				echo 1;
			}else{
				echo 0;
			}
		}
		}

		public function ajaxState(){
			if(request()->isPost()){
			$goodsid = input('post.goods');
			$goods_status = input('post.goodsStatus');
			if($goods_status == 0){
				$goods_status = 1;
			}else{
				$goods_status = 0;
			}
			// pr(goodsModel::state($goodsid,$goods_status));die;
			if(goodsModel::state($goodsid,$goods_status)){
				echo 1;
			}else{
				echo 0;
			}
		}
		}
		private function uploads($pic){
    		
    			$info = $pic->validate(['size'=>300000,'ext'=>'jpg,png,gif,jpeg'])->move(ROOT_PATH. DS. 'public'.DS.'static' . DS . 'uploads');
    			if($info){
			      $newTime = str_replace('-','',date('Y-m-d',time())).'/';

			       $picPath =  'static/uploads/'.$newTime.$info->getFilename();
			       return $picPath;
    			}else{
        		
       				 echo $pic->getError();
    		}
		}
	
		public function ajaxShow(){
			if(request()->isPost()){
			$goodsid = input('post.goods');

			$goodsShow = input('post.goodsShow');
			if($goodsShow==0){
				$goods_show = 1;

			}else{
				$goods_show = 0;
			}
			
			// pr($goodsid);die;
			if(goodsModel::ajaxShow($goodsid,$goods_show)){
				echo 1;
			}else{
				echo 0;
			}
		}
		}

		//商品回收站
		public function recycle(){
					$options    = ['query' =>[]];
					 $wheres['goods_state'] = 1;
		 			$this->assign('goods_name', '');
					if(request()->isGet()){
					$goods_name = input('get.goods_name');

				// pr($goods_name);
				 if ($goods_name) {
				 	$where['goods_name']=array('like',"%{$goods_name}%");
           	
           			 $this->assign('goods_name', $goods_name);
					 $options['query']['goods_name'] = $goods_name;
        		}
		}
			$res = goodsModel::recycle($options,$wheres);
			$num = goodsModel::select(1);
			$type = typeModel::cat();
			// pr($type);
			// pr($res);die;
				$num = count($num);
			if(	$res){

				$this->assign('res',$res);
				$this->assign('type',$type);
				$this->assign('num',$num);
				return $this->fetch();
			}
		}
		
		public function ajaxRecycle(){
			if(request()->isPost()){
				$goods_id = input('post.goods_id');
				$type_id = input('post.type_id');
				$res = goodsModel::ajaxRecycle($goods_id,$type_id);
				// pr($res);die;
				if($res){
					echo 1;
				}else{
					echo 0;
				}

			}
		}

	
}