<?php
namespace app\run\controller;
use app\common\model\Navigation as navigationModel;
use think\Request;
class Navigation extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
	}
	public function index(){
		$res = navigationModel::cat();
		

		if ($res) {
			$num = count($res);
			$this->assign('num',$num);
			$this->assign('res',$res);
		return $this->fetch();
		}
		
	}

	public function add(){
		
		return $this->fetch();
	}
	
	public function insert(){

		if (request()->isPost()) {
			// echo 1;
			$data = input('post.');
			$num = navigationModel::maxs();
			$data['navigation_sort'] = $num+1;
			// pr($data);die;
			$res = navigationModel::add($data);
			if($res){
					$this->redirect('run/navigation/index');
			}
		}
	}

	public function ajaxEdit(){
		$id = input('post.id');
		$newV = input('post.newV');
		$newC = input('post.newC');
		$where['navigation_id'] = $id;
		if($newC == 'navigation_name'){
			
			$data['navigation_name'] = $newV;
		}else{
			// $where['navigation'] = $id;
			$data['navigation_url'] = $newV;
		}
		// pr($data);die;
		$res = navigationModel::ajaxUpdate($data,$where);
		if($res){
			echo 1;
		}else{
			echo 0;
		}
	}

	public function ajaxSort(){
			if(request()->isPost()){
				$navigation_id = input('id');
				$sort = input('sort');
				$navigation_sort  = input('navigation_sort');
				$res = navigationModel::find($sort);
				if($res){
					$where['navigation_id'] = $res['navigation_id'];
					$data['navigation_sort'] = $navigation_sort;
					$res2 = navigationModel::ajaxUpdate($data,$where);
					if($res2){
						$where['navigation_id'] = $navigation_id;
						$data['navigation_sort'] = $sort;
						$res3 = navigationModel::ajaxUpdate($data,$where);
						if($res3){
							echo $res['navigation_id'];
						}else{
							echo 0;
						}
					}
				}
			}
	}
}
