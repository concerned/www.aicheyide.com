<?php
namespace app\run\controller;
use app\common\model\Configure as configureModel;
class Configure extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
	}
	public function index(){
		
		return $this->fetch();
	}

	public function add(){
		if(request()->isGet()){
			
		$this->assign('error','');
		$goods_id =	input('goods_id');
		$res = configureModel::adds($goods_id);
		// pr($res[0]);die;
			if($res){
				$this->assign('res',$res);
					return $this->fetch();
			}else{
				// $this->assign('error','没有该商品');
					return $this->fetch();
			}
		}
	

	}
	
	public function insert(){
		if(request()->isPost()){
			$data = input('post.');
			if(configureModel::inserts($data)){
				$this->redirect('run/goods/index');
			}else{
				$this->assign('error','添加商品配置失败');
				$this->redirect('run/configure/add',array('goods_id'=>$data['goods_id']));
			}
		}
	}

	public function update(){
		if(request()->isPost()){
				$data = input('post.');
				if(configureModel::updates($data)){
				$this->redirect('run/goods/index');
			}else{
				$this->assign('error','修改商品配置失败');
				$this->redirect('run/configure/add',array('goods_id'=>$data['goods_id']));
			}
		}
	}
}