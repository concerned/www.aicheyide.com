<?php
namespace app\run\controller;
use app\common\model\Menu;
use think\Db;
class Menus extends Run{
	
	public function _initialize(){
			
		call_user_func(array('parent',__FUNCTION__));
	}
	public function lists()
	    {
	    	$this->addJs('/run/js/bootstrap-treeview.min');
	    	$sel = [];
	    	$result =Db::name('menu')->select();

	    	$this->assign('result',$result);
	    	
	    	
	    	/*$Category = new Menu();//实例化类
	    	$category_tree = $Category->category_tree();// 获取整体多级数组树结果
	    	$this->view->category_list = $Category->category_html($category_tree);//将结果转化为实体html并传值给模板
	    	$sel = Db::name('menu')->select();*/
	        return $this->fetch();
	    }
	public function add(){
		 
		if(request()->isPost()){
			$data = input('post.');
			$result = Menu::create($data);

		}
		return $this->fetch();
	}
}