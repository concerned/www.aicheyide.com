<?php
namespace app\run\controller;
use think\Db;
class Login extends Run{
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));
	}

	public function login(){
		 $this->assign('error','');
	
		if(request()->isPost()){
            $err = array();
            $name = trim(input('post.users_name'));

            $password = trim(input('post.users_password'));
            $vcode =trim(input('post.vcode'));
            $re = $this->check($vcode);
            if($re==true){
               $sel = Db::table('lo_admin_users')->where(array('users_name'=>array('eq',$name)))->find();
          

            if($sel){
                if($sel['users_status']!=1){
                    $this->error('当前用户不存在或被禁用','login');
                } else if(md5($password)==$sel['users_password']){
                    // $sel['Now_time'] =date('Y-m-d H:i:s');
                    session('RUN_USER',$sel);

                     $this->redirect('run/Index/index');

                   
                }
                else{
                     $this->assign('error','密码错误,请重新输入');
                }
            }
            else{
                $this->assign('error','没有此用户,请重新输入');
            } 
            }else{
                $this->assign('error','验证码错误,请重新输入');
            }
            


            }
            return $this->fetch();
         }
 
     // 根据post的值来检测验证码
	    private  function check($code='')
	    {
	        if (!captcha_check($code)) {
	            return false;
	        } else {
	            return true;
	        }
	    }
	
		public function loginOut(){
			session('RUN_USER',null);
	        $this->redirect('Login/login');	

		}

}