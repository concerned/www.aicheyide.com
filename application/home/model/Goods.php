<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;
class Goods extends Model{//属性表
	public static function goodsDetails(){

		 $sel  =Db::name('goods_details')->alias('a')
        ->join('goods_types b','a.type_id=b.type_id')
        ->field('a.*,b.type_id,b.type_name,b.type_pinyin')
        ->where('a.goods_state',0)->order('goods_status asc')->select();//关联类别表
		return $sel;
	}
	public static function carInfo(){
		$resAll = [];
		$res = Db::name('car_brand_models_dict')->alias('a')
		->join('car_brand_dict b','a.brand_id=b.id')
		->field('a.names,b.first_letter,b.name')
		->order('b.first_letter asc')
		 
		 
		->select();
 		// $resA = ['A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
 		foreach($res as $key=>$value){
 				 $resAll[$value['first_letter']][$value['first_letter']][$value['name']][$value['names']] = $value['names'];

 			}
 			
		return $resAll;
	}



	public static function carSel($resA,$res){
		$resAll='';

		foreach($resA as $k=>$v){
			$resAll[] =$v; 
		}
	/*	foreach($data as $k=>$v){
			$resAll['names']=$v['names'];
		}*/
 

		return $resAll;
	}
	 //提交意向车型\
	 public static function ajaxIntention($data){
	 	$res = Db::name('intention')->insert($data);
	 	if($res){
	 		return $res;
	 	}
	 }

	 

}