<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;

class Login extends Model{
		
		public static function login($data){
			$res = Db::name('login_info')->insert($data);
			return $res;
		}

		public static function cat($phone){
			$where['login_phone'] = $phone;
			$where['login_state'] = 0;
			$res = Db::name('login_info')->where($where)->find();
			return $res;
		}

		public static function cats($phone){
			$where['login_phone'] = $phone;
			
			$res = Db::name('login_info')->where($where)->find();
			return $res;
		}
		public static function updates($data){
			$res = Db::name('login_info')->update($data);
			return $res;
		}
}