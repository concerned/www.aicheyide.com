<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;

class Problem extends Model{
	public static function select($where){
		$res = Db::name('problem')->where($where)->select();
		return $res;
	}
	
}