<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;
class Follow extends Model{//属性表
	public static function followView($sessionUser){
		if(session('PHONE')!=''){
			$resAll =[]; 
			$res= Db::name('follow')->alias('a')
				->join('goods_details b','a.goods_id=b.goods_id')
				->where('user_phone',$sessionUser)
				->field('a.goods_id,b.goods_pic,b.goods_name,b.goods_model,b.goods_describe,b.goods_car,b.goods_payment,b.goods_month,b.goods_price')
				->select();
			if($res){
				return $res;
			}

		}
		
	}
}