<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;
class Details extends Model{//属性表
	public static function goodsDetails($goods_id){

		$sel=Db::name('goods_details')->alias('a')
        ->join('goods_configure b','a.goods_id=b.goods_id')
        ->where('a.goods_id',$goods_id)->find();//关联类别表
		return $sel;
	}
	/**
	* 详情页面门店的查询操作
	* @access public 
	* @return array 需要显示的门店
	*/  
	public static function storeSelect()
	{
		$res = Db::name('store')->where(array('store_state'=>array('eq',0)))->select();
		if(!$res)return false;
		return $res;
	}
	//循环门店
	public static function storeCache()
	{
		$resAll=array();
		$res = Db::name('store')->where('store_level',1)->select();

		foreach($res as $k=>$v){
			 
			$resAll[$v['store_name']] = Db::name('store')->field('store_name')->where('store_uid',$v['store_id'])->select();
		}
		return $resAll;
	}
	//是否预约
	public static function IsYuYue($typeid){
		if(session('PHONE')!=''){
			$where['user_phone'] = session('PHONE');
			$where['goods_id'] = $typeid;
			$res = Db::name('subscribe')->field('goods_id')->where($where)->find();
			 
			return $res;
			 
		}
	}
	//是否是关注状态
	public static function isGuanZhu($goodsId){
		if(session('PHONE')!==''){
			$where['user_phone']=session('PHONE');
			$where['goods_id']=$goodsId;
				
			$res = Db::name('follow')->field('goods_id')->where($where)->find();
			return $res;
		}
	}
	//预约次数
	public static function yuYueNum(){
		if(session('PHONE')!=''){
	
			$res = Db::name('subscribe')->field('goods_id')->where('user_phone',session('PHONE'))->select();
			 
			return $res;
			 
		}
	}
	//随机推荐
	
	
	public static function randGoods(){
		$sql = " SELECT goods_id,goods_name,goods_model,goods_describe,goods_car,goods_pictures,goods_price,goods_month,goods_payment FROM `lo_goods_details` AS t1 JOIN (SELECT ROUND(RAND() * ((SELECT MAX(goods_id) FROM `lo_goods_details`)-(SELECT MIN(goods_id) FROM `lo_goods_details`))+(SELECT MIN(goods_id) FROM `lo_goods_details`)) AS id) AS t2 WHERE t1.goods_id >= t2.id AND t1.goods_state=0 AND t1.goods_status=0 ORDER BY t1.goods_id LIMIT 4";
		$res = Db::query($sql);


		return $res;
	}


}