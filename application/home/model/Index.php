<?php 
namespace app\home\model;
use app\run\model\App;
use think\Model;
use think\Db;

class Index extends Model{
	public static function navigationSelect(){
		$res = Db::name('navigation')->order('navigation_sort','asc')->select();
		// pr($res);
		return $res;
	}
	

	public static function goodsSelect($where){
		$res = Db::name('goods_details')->where($where)->limit(0,6)->order('goods_id','desc')->select();
		// pr($res);
		return $res;
	}
}