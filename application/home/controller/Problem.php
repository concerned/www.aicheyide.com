<?php
namespace app\home\controller;
use app\home\model\Problem as problemModel;
class Problem extends Home
{

    public function index()
    {
          if($this->dies == true){
            $this->redirect('/dies');
        }
    	$this->addCss('/home/css/fang/questions');
    	$where['problem_state'] = 1;
     	$res = problemModel::select($where);
     	if($res){
     		$this->assign('res',$res);
     	}
    	return $this->fetch();
    }
}
