<?php
namespace app\home\controller;
use app\home\model\Goods as goodsModel;

use think\Db;
// use think\Rquest;
class Type extends Home
{
    public function _initialize(){
   
        call_user_func(array('parent',__FUNCTION__));
    }
    public function index()
    {     if($this->dies == true){
            $this->redirect('/dies');
        }
         
        $this->addCss('/home/css/type_lists');
        $this->addCss('/home/css/fang/list');
        $this->addCss('/home/css/fang/paging');
        $this->addCss('/home/css/fang/component');
     	$this->addCss('/home/css/fang/my');
        // $this->addJs('/home/js/fang/list');
        // $this->addJs('/home/js/fang/common');

        $this->addJs('/home/js/fang/paging');
        // $this->addJs('/home/js/layer/layer');
        //城市
        $this->addJs('/home/js/fang/distpicker.data.min');
        $this->addJs('/home/js/fang/distpicker');
        // $this->addJs('/home/js/fang/main');
        //layui
        // $this->addJs('/run/lib/layui/layui');

        
         // 分类查询
     	$tpyeLists =Db::name('goods_types')->where(function($query){
            $query->where('type_state',0);
        })->select();
        $this->assign('tpyeLists',$tpyeLists);
       
       
       
        $typeAttribute = goodsModel::goodsDetails();
        // pr($typeAttribute);
        // pr($typeAttribute);
        $typeAttributes = $typeAttribute;
        foreach ($typeAttributes as $key => $value) {
            $typeAttributes[$key]['typeIsChoosen']='YES';
            $typeAttributes[$key]['sfIsChoosen']='YES';
            $typeAttributes[$key]['ygIsChoosen']='YES';
            $typeAttributes[$key]['cxIsChoosen']='YES';
        }
        // pr($typeAttributesibutes);
        $typeAttributes = json_encode($typeAttributes);
        $this->assign('typeAttributes',$typeAttributes);
        $this->assign('typeAttribute',$typeAttribute);
        //车辆配置
    	return $this->fetch();
    }
    public function isSession(){
        if(request()->isGet()){
            if(!session('PHONE')){
                return 1;
            }else{
                return session('PHONE');
            }
        }
    }
    //提交意向车型
    public function ajaxIntention(){
         if(request()->isPost()){
            $data = input('post.');
            $res = goodsModel::ajaxIntention($data);
            if($res){
                return 1;
            }
         }
    }
/*    array(
        ['D']=array(
                'name'=>array(
                    'names'=>array(
                        [0]=>'polo',
                        [1]=>'桑塔纳',
                        [2]=>
                        )
                        
                    )
            )
        )*/

}
