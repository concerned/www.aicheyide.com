<?php
namespace app\home\controller;
use app\home\model\Index as indexModel;
use app\home\model\Problem as problemModel;
use app\common\model\Website as websiteModel;
use think\Db;
class Index extends Home
{

    public function index()
    {
     	// $this->addCss('/home/css/animation');
     	// $this->addCss('/home/css/index');
     	// $this->addCss('/home/css/fang/animation');
     	// $this->addCss('/home/css/fang/index');
    
        // $this->addCss('/home/css/fang/swiper-3.4.0.min');
        $this->addCss('/home/css/fang/font-awesome.min');
        
        // $this->addCss('/home/css/fang/animate.min');
        $this->addCss('/home/css/fang/bootstrap-touch-slider');
        
        // $this->addJs('/home/js/fang/swiper-3.4.0.jquery.min');
      

        // $this->addJs('/home/js/fang/html5zoo');
        // $this->addJs('/home/js/fang/lovelygallery');
        // $this->addJs('/home/js/fang/index');
        
     	// 聊天插件
     	//首页问题查询
        if($this->dies == true){
            return $this->fetch('dies');
        }
        $where['problem_state'] = 0;
        $poblem = problemModel::select($where);

        //首页商品查询
        $wheres['goods_show'] = 0;
        $wheres['goods_state'] = 0;
        $goods  = indexModel::goodsSelect($wheres);
        $this->assign('poblem',$poblem);
        $this->assign('goods',$goods);
        // pr($goods);
    	return $this->fetch();
    }
    public function dies(){

        return $this->fetch();
         
    }
}
