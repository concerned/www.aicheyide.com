<?php
namespace app\home\controller;
use app\home\model\Follow as followModel;
use think\Db;
class Follow extends Home
{
	public function _initialize(){
 			call_user_func(array('parent',__FUNCTION__));
	}

    public function index()
    {
          if($this->dies == true){
            $this->redirect('/dies');
        }
    	$sessionFollow = session('PHONE');
   		$res = followModel::followView($sessionFollow);
        // pr($res);
        $ress = Db::name('subscribe')->select();

   		$this->assign('res',$res);
    	return $this->fetch();
    }
    //加入关注
    public function ajaxFollow(){
    	if(request()->isGet()){
            $data['goods_id']= input('goods_id');
    		$data['user_phone']= input('user_phone');
    		 
    		$res = Db::name('follow')->insert($data);
    		if($res){
    			return 1;
    		}
    	}
    	
    }
    //删除关注
    public function ajaxDel(){
        if(request()->isPost()){
            $id = input('post.');
            $res = Db::name('follow')->where($id)->delete();
            if($res){
                return 1;
            }
        }
    }
   public function delFollow(){
        if(request()->isGet()){
            $data['goods_id']= input('goods_id');
            $data['user_phone']= input('user_phone');
            $res = Db::name('follow')->where($data)->delete();
            if($res){
                return 1;
            }
        }
    }

}



