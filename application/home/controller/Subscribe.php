<?php
namespace app\home\controller;
use app\home\model\Subscribe as SubscribeModel;
use think\Db;
class Subscribe extends Home
{

    public function _initialize(){
        call_user_func(array('parent',__FUNCTION__));
    }

    public function index()
    {  
         if($this->dies == true){
        $this->redirect('/dies');
        }
        $sess = session('PHONE');
        $res = SubscribeModel::selSubs($sess);
        // $res = Db::name('subscribe')->find();
        $resAll = [];
        foreach($res as $k=>$v){
            $res[$k]['store_name']=Db::name('store')->field('store_company,store_phone')->where('store_name',$v['subscribe_ctiy_name'])->find();
 
        }
         
        $this->assign('res',$res);
    	return $this->fetch();
    }
    //预约信息入库
    public function ajaxYuYue(){
    	if(request()->isPost()){
            $yuyueArr=input('post.');

           	$strtime = input('post.subscribe_time');
    		$yuyueArr['subscribe_time'] = strtotime($strtime,time());
            

         	//return json_encode($yuyueArr);//这里有数据

    		// $yuyueArr['subscribe_time'] = strtotime($yuyueArr['subscribe_time'],time());
    		$res = Db::name('subscribe')->insert($yuyueArr);//打印sql语句
    	    if($res){
                return 1;
            }else{
                return 0;
            }
    	}
    }

}
