<?php 
namespace app\home\controller;
use app\common\controller\App;
use app\home\model\Index as indexModel;
use app\home\model\Goods as goodsModel;
use app\home\model\Details as DetailsModel ;
use app\common\model\Website as websiteModel;

use think\Db;

class Home extends App{
	public $dies=false;
	public function _initialize(){
		
		call_user_func(array('parent',__FUNCTION__));

			$this->assign('session',session('PHONE'));
			// pr(session('PHONE'));
			$this->addJs('/home/js/layer/layer');
			   $this->addCss('/home/css/fang/index');
			$navigation = indexModel::navigationSelect();
			$this->assign('navigation',$navigation);
			// 当前控制器
			$this->assign('controllerActive',strtolower($this->params['controller']));
			//汽车配置
		 	$res = goodsModel::carInfo();
        	cache('CARINFO',$res);
        	$this->assign('carinfo',cache('CARINFO'));
        	//预约门店
			$_storeache = DetailsModel::storeCache();
			// pr($_storeache);
	 		cache('CACHE_STORE',$_storeache);
	 		$this->assign('storecache',cache('CACHE_STORE'));
	 		//预约数量
	 		$yuYueNum = DetailsModel::yuYueNum();
			$this->assign('yuYueNum',$yuYueNum);
			//网站底部年份
			$this->assign('time',date('Y'));
			//关键字、关键内容
		 	$state = websiteModel::index();
			  if($state['website_state']==0){
            $this->dies=true;
            
           
         }
			$cacheState = cache('website',$state);
			$this->assign('cacheState',cache('website'));
			//手机号中间四位*号代替
			 
			$this->assign('replacePhone',substr(session('PHONE'), 0, 3).'****'.substr(session('PHONE'), 7));
        	
	}
}