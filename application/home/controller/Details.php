<?php
namespace app\home\controller;
use app\home\model\Details as DetailsModel ;
use think\Db;
class Details extends Home
{
	public function index(){
 		  if($this->dies == true){
            $this->redirect('/dies');
        }
		$this->addCss('/home/css/fang/jquery.datetimepicker');
		$this->addCss('/home/css/fang/swiper-3.4.0.min');
		$this->addCss('/home/css/fang/detail');

		if(request()->isGet()){
			$typeId = input('goods_id');
			

			$reuslt = DetailsModel::goodsDetails($typeId);
			$reuslt['goods_pictures'] = explode(',', $reuslt['goods_pictures']);

			 
			$car = Db::name('goods_highlights')->field('car_img,car_title,car_content')->where('goods_id',$typeId)->select();
			$res = array();
			 
			foreach($car as $k => $v){
				// pr($k);
				$res[$k]= $v;
			}
			$reuslt['goods_highlights']=$res;
			 
			$this->assign('reuslt',$reuslt);
			//扔出用户是否预约变量
			$isYuYue = DetailsModel::IsYuYue($typeId);
			$this->assign('isYuYue',$isYuYue);
			 //是否是关注状态
        	$isFollow = DetailsModel::isGuanZhu($typeId);
        	 $this->assign('isFollow',$isFollow);
			

			//门店信息查询
			$res = DetailsModel::storeSelect();
			$this->assign('res',$res);
			//展示到预约模态框
			/*$_storeache = DetailsModel::storeCache();
			//查询出来存到缓存
	 		cache('CACHE_STORE',$_storeache);
	 		*/
	 		
	 		//随机查询
	 		$randGoods = DetailsModel::randGoods();
	 		foreach($randGoods as $k=>$v){
	 			 $randGoods[$k]['goods_pictures'] = explode(',', $v['goods_pictures']);	
	 		}
	 		 // pr($randGoods);//打印结果
	 		$this->assign('randgoods',$randGoods);
		 

		}
		return $this->fetch();
		
	}
	public function buhuo(){
		if(request()->isGet()){
			$id = input('goods_id');
			$res = Db::name('goods_details')->field('goods_status')->where('goods_id',$id)->find();
			echo json_encode($res);
			// pr($res['goods_status']);
			 
		}
	}
	 
	public function isYuYue(){

	}


}