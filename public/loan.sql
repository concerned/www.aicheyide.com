/*
Navicat MySQL Data Transfer

Source Server         : 2
Source Server Version : 50553
Source Host           : 192.168.10.239:3306
Source Database       : loan

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-09-19 18:07:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for lo_admin_users
-- ----------------------------
DROP TABLE IF EXISTS `lo_admin_users`;
CREATE TABLE `lo_admin_users` (
  `users_id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `users_name` char(10) NOT NULL COMMENT '管理员名称',
  `users_passwrod` char(80) NOT NULL COMMENT '管理员密码',
  `users_haderimg` char(80) NOT NULL COMMENT '管理员头像',
  `users_phone` tinyint(4) NOT NULL COMMENT '管理员电话',
  `users_status` tinyint(4) NOT NULL COMMENT '管理员状态',
  `users_ip` int(10) unsigned NOT NULL COMMENT '管理员IP',
  PRIMARY KEY (`users_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_admin_users
-- ----------------------------

-- ----------------------------
-- Table structure for lo_goods_configure
-- ----------------------------
DROP TABLE IF EXISTS `lo_goods_configure`;
CREATE TABLE `lo_goods_configure` (
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `car_structure` char(30) CHARACTER SET utf8 NOT NULL COMMENT '车身结构',
  `car_engine` char(30) CHARACTER SET utf8 NOT NULL COMMENT '发动机',
  `car_oil` char(30) CHARACTER SET utf8 NOT NULL COMMENT '综合油耗',
  `car_size` char(16) CHARACTER SET utf8 NOT NULL COMMENT '车身尺寸',
  `car_gearbox` char(30) CHARACTER SET utf8 NOT NULL COMMENT '变速箱',
  `car_fuel` char(20) CHARACTER SET utf8 NOT NULL COMMENT '燃料形式',
  `car_color` char(30) CHARACTER SET utf8 NOT NULL COMMENT '车辆配色',
  PRIMARY KEY (`goods_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lo_goods_configure
-- ----------------------------

-- ----------------------------
-- Table structure for lo_goods_details
-- ----------------------------
DROP TABLE IF EXISTS `lo_goods_details`;
CREATE TABLE `lo_goods_details` (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `goods_name` char(50) CHARACTER SET utf8 NOT NULL COMMENT '商品名称',
  `goods_model` char(7) CHARACTER SET utf8 NOT NULL COMMENT '商品款型',
  `goods_describe` char(50) CHARACTER SET utf8 NOT NULL COMMENT '商品描述',
  `goods_price` decimal(5,2) unsigned NOT NULL COMMENT '厂商指导价',
  `goods_payment` decimal(4,2) unsigned NOT NULL COMMENT '首付',
  `goods_month` tinyint(6) unsigned NOT NULL COMMENT '月供',
  `goods_car` char(10) CHARACTER SET utf8 NOT NULL COMMENT '车型',
  `goods_retainage` tinyint(7) NOT NULL COMMENT '尾款购车',
  `type_id` int(10) unsigned NOT NULL COMMENT '类型id',
  `goods_pic` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '封面图片',
  `goods_pictures` text CHARACTER SET utf8 NOT NULL COMMENT '多图片',
  PRIMARY KEY (`goods_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lo_goods_details
-- ----------------------------

-- ----------------------------
-- Table structure for lo_goods_types
-- ----------------------------
DROP TABLE IF EXISTS `lo_goods_types`;
CREATE TABLE `lo_goods_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_name` char(10) CHARACTER SET utf8 NOT NULL COMMENT '分类名',
  `type_pic` char(80) CHARACTER SET utf8 NOT NULL COMMENT '分类图片',
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lo_goods_types
-- ----------------------------

-- ----------------------------
-- Table structure for lo_home_users
-- ----------------------------
DROP TABLE IF EXISTS `lo_home_users`;
CREATE TABLE `lo_home_users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `users_phone` char(11) NOT NULL COMMENT '用户电话',
  `users_email` char(30) NOT NULL COMMENT '用户邮箱',
  `users_state` char(1) NOT NULL COMMENT '用户状态',
  `users_password` char(80) NOT NULL COMMENT '用户密码',
  `users_addtime` datetime NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `users_phone` (`users_phone`),
  UNIQUE KEY `users_email` (`users_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_home_users
-- ----------------------------

-- ----------------------------
-- Table structure for lo_home_usersinfo
-- ----------------------------
DROP TABLE IF EXISTS `lo_home_usersinfo`;
CREATE TABLE `lo_home_usersinfo` (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `user_name` char(30) NOT NULL COMMENT '用户名',
  `user_address` varchar(255) NOT NULL COMMENT '用户住址',
  `user_age` tinyint(2) NOT NULL COMMENT '年龄',
  `user_sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别，0为女，1为男',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_home_usersinfo
-- ----------------------------

-- ----------------------------
-- Table structure for lo_navigation
-- ----------------------------
DROP TABLE IF EXISTS `lo_navigation`;
CREATE TABLE `lo_navigation` (
  `navigation_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '前台导航id',
  `navigation_name` char(20) NOT NULL COMMENT '前台导航名称',
  `navigation_url` char(50) NOT NULL COMMENT '前台导航网址',
  PRIMARY KEY (`navigation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_navigation
-- ----------------------------

-- ----------------------------
-- Table structure for lo_problem
-- ----------------------------
DROP TABLE IF EXISTS `lo_problem`;
CREATE TABLE `lo_problem` (
  `
problem_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '问题id',
  `problem_name` varchar(255) NOT NULL COMMENT '问题',
  `problem_answer` text NOT NULL COMMENT '答案',
  PRIMARY KEY (`
problem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lo_problem
-- ----------------------------
