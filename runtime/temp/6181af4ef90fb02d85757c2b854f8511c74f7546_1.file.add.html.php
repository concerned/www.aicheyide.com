<?php /* Smarty version 3.1.27, created on 2017-09-17 18:12:59
         compiled from "E:\phpstudy\WWW\office\application\run\view\menus\add.html" */ ?>
<?php
/*%%SmartyHeaderCode:1570459be4aab66cbd0_40487293%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6181af4ef90fb02d85757c2b854f8511c74f7546' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\menus\\add.html',
      1 => 1505643174,
      2 => 'file',
    ),
    'a5f265850ccf14c6541f2361ba1aabeb721c61f8' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\global.html',
      1 => 1505630746,
      2 => 'file',
    ),
    '72e4285e7dea1da41bf34efa363a8216162209fc' => 
    array (
      0 => '72e4285e7dea1da41bf34efa363a8216162209fc',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '1570459be4aab66cbd0_40487293',
  'variables' => 
  array (
    'css' => 0,
    'each' => 0,
    'absroot' => 0,
    'js' => 0,
    'static' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59be4aab7c6694_73425945',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59be4aab7c6694_73425945')) {
function content_59be4aab7c6694_73425945 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1570459be4aab66cbd0_40487293';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title></title>
    <link rel = "Shortcut Icon" href=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=0">

    
    <?php if ($_smarty_tpl->tpl_vars['css']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['css']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php } else { ?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['js']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php } else { ?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>
   
</head>
 
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('run/Index/index');?>
">后台管理系统V1.0</a>

        </div>
		
        <div class="header-right">

            
            <a href="<?php echo url('home/Index/index');?>
" class="btn btn-primary" title="访问前台">访问前台<i class="glyphicon glyphicon-home"></i></a>
            <a href="login.html" class="btn btn-danger" title="退出登陆">退出<i class="glyphicon glyphicon-log-in"></i></a>

        </div>
    </nav>
    <nav class="navbar-default navbar-side" role="navigation">

        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
				<li>
				    <div class="user-img-div">
				        <img src="<?php echo $_smarty_tpl->tpl_vars['static']->value;?>
images/img29.jpg" class="img-thumbnail" />

				        <div class="inner-text">
				            超级管理员
				        <br />
				            <small>登陆次数 : 2  </small>
				            <small>上次登陆 : 2017-9-16 </small>
				        </div>
				    </div>

				</li>
                <li>
                    <a class="active-menu" href="<?php echo url('run/Menus/lists');?>
"><i class="fa fa-dashboard "></i>导航 / 模块管理</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-desktop "></i>UI Elements <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="panel-tabs.html"><i class="fa fa-toggle-on"></i>Tabs & Panels</a>
                        </li>
                        <li>
                            <a href="notification.html"><i class="fa fa-bell "></i>Notifications</a>
                        </li>
                         <li>
                            <a href="progress.html"><i class="fa fa-circle-o "></i>Progressbars</a>
                        </li>
                         <li>
                            <a href="buttons.html"><i class="fa fa-code "></i>Buttons</a>
                        </li>
                         <li>
                            <a href="icons.html"><i class="fa fa-bug "></i>Icons</a>
                        </li>
                         <li>
                            <a href="wizard.html"><i class="fa fa-bug "></i>Wizard</a>
                        </li>
                         <li>
                            <a href="typography.html"><i class="fa fa-edit "></i>Typography</a>
                        </li>
                         <li>
                            <a href="grid.html"><i class="fa fa-eyedropper "></i>Grid</a>
                        </li>
                        
                       
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-yelp "></i>Extra Pages <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="invoice.html"><i class="fa fa-coffee"></i>Invoice</a>
                        </li>
                        <li>
                            <a href="pricing.html"><i class="fa fa-flash "></i>Pricing</a>
                        </li>
                         <li>
                            <a href="component.html"><i class="fa fa-key "></i>Components</a>
                        </li>
                         <li>
                            <a href="social.html"><i class="fa fa-send "></i>Social</a>
                        </li>
                        
                         <li>
                            <a href="message-task.html"><i class="fa fa-recycle "></i>Messages & Tasks</a>
                        </li>
                        
                       
                    </ul>
                </li>
                <li>
                    <a href="table.html"><i class="fa fa-flash "></i>Data Tables </a>
                    
                </li>
                 <li>
                    <a href="#"><i class="fa fa-bicycle "></i>Forms <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                       
                         <li>
                            <a href="form.html"><i class="fa fa-desktop "></i>Basic </a>
                        </li>
                         <li>
                            <a href="form-advance.html"><i class="fa fa-code "></i>Advance</a>
                        </li>
                         
                       
                    </ul>
                </li>
                  <li>
                    <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>
                </li>
                 <li>
                    <a href="error.html"><i class="fa fa-bug "></i>Error Page</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-sign-in "></i>Login Page</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap "></i>Multilevel Link <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="#"><i class="fa fa-bicycle "></i>Second Level Link</a>
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-flask "></i>Second Level Link</a>
                        </li>
                        <li>
                            <a href="#">Second Level Link<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="#"><i class="fa fa-plus "></i>Third Level Link</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-comments-o "></i>Third Level Link</a>
                                </li>

                            </ul>

                        </li>
                    </ul>
                </li>
               
                <li>
                    <a href="blank.html"><i class="fa fa-square-o "></i>Blank Page</a>
                </li>
            </ul>

        </div>

    </nav>
<div>
 <style type="text/css">
	
.breadcrumb > li + li:before {
   color: green;
  content: "/ ";
   padding: 0 5px;
}


 </style>

<div id="page-wrapper">

<?php
$_smarty_tpl->properties['nocache_hash'] = '1570459be4aab66cbd0_40487293';
?>

<p class="bg-success">
 <a href="javascript:history.go(-1)" class="btn btn-info" title=" "> <i class="glyphicon glyphicon-backward"> </i> 返回上一级</a>
</p>
<div class="container">
 <div class="row">
          <div class="col-lg-8">
              <form id="defaultForm" method="post" class="form-horizontal" action="">
                  <div class="form-group">
                      <label class="col-lg-3 control-label">栏目标题</label>
                      <div class="col-lg-5">
                          <input type="text" class="form-control" name="catname" placeholder="栏目标题" />
                      </div>
                    
                  </div>

                  <div class="form-group">
                      <label class="col-lg-3 control-label">URL链接</label>
                      <div class="col-lg-5">
                          <input type="text" class="form-control" name="url" placeholder="只限字母组成,格式如:ad/show" />
                      </div>
                  </div>

    <div class="form-group">

        <label class="col-lg-3 control-label">
        导航是否显示</label>
        <div class="col-lg-5">
        <input type="checkbox"  checked  onchange="gcheck('g_title');" id="g_title"  class="fchen_control" data-size="small" />
        </div>
    </div>
                 <div class="form-group">
                     <label class="col-lg-3 control-label">排序权重</label>
                     <div class="col-lg-5">
                         <input type="number" class="form-control" name="displayorder" step="1"
                                min="0" data-bv-greaterthan-inclusive="false" data-bv-greaterthan-message="最小排序为1"
                                max="100" data-bv-lessthan-inclusive="true" data-bv-lessthan-message="最大排序为100"
                                 />
                         <!--<input type="range" class="form-control" name="age"
                                required
                                min="10" max="100" data-bv-between-message="The input must be between 10 and 100"
                                 />-->
                     </div>
                 </div>
                  <div class="form-group">
                      <div class="col-lg-9 col-lg-offset-3">
                          <button type="submit" class="btn btn-primary" name=" " value="Sign up">添加</button>
                      </div>
                  </div>
                  <input type="hidden" name="upid" value="0">
                  <input type="hidden" name="status" class="status" value="1">
              </form>
          </div>
      </div>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
function gcheck(a){
        var a;
        var $aa = $("#"+a);
            //console.log($aa.is(":checked"));  //调试代码
        if($aa.is(":checked")==true){
            $('.status').val(1);
        }else{
 
            $('.status').val(0);
        }
    }
$("#g_title").bootstrapSwitch();
$(document).ready(function() {
    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
        container: 'tooltip',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            catname: {

                group: '.col-lg-5',
                validators: {
                    notEmpty: {
                        message: '标题不能为空'
                    }
                }
            },
          
            displayorder:{ group: '.col-lg-5',
                validators: {
                  notEmpty: {
                          message: '不能为空！'
                      },
                  stringLength: {
                          min: 1,
                          max: 2,
                          message: '长度必须在2个字符以内！'
                      },
                }
            },
            url: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: '不能为空！'
                    },
                    stringLength: {
                        min: 2,
                        max: 30,
                         
                        message: '长度2-30位'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z_\.]+$/,
                        message: '请输入合法字母'
                    }
                   
                }
            },
           
         
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            }
        }
    });
});
<?php echo '</script'; ?>
>

</div>
</body>
</html><?php }
}
?>