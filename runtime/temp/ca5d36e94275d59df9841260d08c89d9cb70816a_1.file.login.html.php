<?php /* Smarty version 3.1.27, created on 2017-09-19 18:06:47
         compiled from "E:\phpStudy\WWW\loan\application\run\view\user\login.html" */ ?>
<?php
/*%%SmartyHeaderCode:48159c0ec370f2b97_28479129%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ca5d36e94275d59df9841260d08c89d9cb70816a' => 
    array (
      0 => 'E:\\phpStudy\\WWW\\loan\\application\\run\\view\\user\\login.html',
      1 => 1505815515,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '48159c0ec370f2b97_28479129',
  'variables' => 
  array (
    'run' => 0,
    'static' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59c0ec37140da9_12244213',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59c0ec37140da9_12244213')) {
function content_59c0ec37140da9_12244213 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '48159c0ec370f2b97_28479129';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>后台登录-X-admin1.1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
css/font.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
css/xadmin.css">
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
css/swiper.min.css">
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['static']->value;?>
jquery-1.11.1.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
js/swiper.jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
lib/layui/layui.js" charset="utf-8"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
js/xadmin.js"><?php echo '</script'; ?>
>

</head>
<body>
    <div class="login-logo"><h1>X-ADMIN V1.1</h1></div>
    <div class="login-box">
        <form class="layui-form layui-form-pane" action="<?php echo url('run/User/toLogin');?>
" method="post">
              
            <h3>登录你的帐号</h3>
            <label class="login-title" for="username">帐号</label>
            <div class="layui-form-item">
                <label class="layui-form-label login-form"><i class="iconfont">&#xe6b8;</i></label>
                <div class="layui-input-inline login-inline">
                  <input type="text" name="users_name" lay-verify="required" placeholder="请输入你的帐号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <label class="login-title" for="password">密码</label>
            <div class="layui-form-item">
                <label class="layui-form-label login-form"><i class="iconfont">&#xe82b;</i></label>
                <div class="layui-input-inline login-inline">
                  <input type="text" name="users_password" lay-verify="required" placeholder="请输入你的密码" autocomplete="off" class="layui-input">
                </div>
            </div>
             <label class="login-title" for="username">验证码</label>
            <div class="layui-form-item">
                <label class="layui-form-label login-form"><i class="layui-icon">&#xe60d;</i></label>
                <div  class="layui-input-inline login-inline" style="width:40%">
                  <input type="text"  name="vcode" lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input">
                </div>
                
                    <a  href="javascript:;" style="float:right;display:block"><img src="<?php echo captcha_src();?>
" onclick="this.src='<?php echo captcha_src();?>
?'+Math.random();" width="120" height="35" alt="captcha" style="border-radius: 5px;"/></a>
                
            </div>
            <div class="form-actions" style="margin-top:44px">
                <button class="btn btn-warning pull-right" lay-submit lay-filter="login"  type="submit">登录</button> 
               
            </div>
        </form>
    </div>
    <div class="bg-changer">
        <div class="swiper-container changer-list">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/a.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/b.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/c.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/d.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/e.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/f.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/g.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/h.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/i.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/j.jpg" alt=""></div>
                <div class="swiper-slide"><img class="item" src="<?php echo $_smarty_tpl->tpl_vars['run']->value;?>
images/k.jpg" alt=""></div>
                <div class="swiper-slide"><span class="reset">初始化</span></div>
            </div>
        </div>
        <div class="bg-out"></div>
        <div id="changer-set"><i class="iconfont">&#xe696;</i></div>   
    </div>
    <?php echo '<script'; ?>
>
        // $(function  () {
        //     layui.use('form', function(){
        //       var form = layui.form();
        //       //监听提交
        //       form.on('submit(login)', function(data){
        //         layer.msg(JSON.stringify(data.field),function(){
        //             location.href='index.html'
        //         });
        //         return false;
        //       });
        //     });
        // })
        
    <?php echo '</script'; ?>
>
    
</body>
</html><?php }
}
?>