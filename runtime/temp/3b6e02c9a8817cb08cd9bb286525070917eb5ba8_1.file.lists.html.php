<?php /* Smarty version 3.1.27, created on 2017-09-17 21:30:20
         compiled from "E:\phpstudy\WWW\office\application\run\view\menus\lists.html" */ ?>
<?php
/*%%SmartyHeaderCode:338459be78eccb6bb7_39116734%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b6e02c9a8817cb08cd9bb286525070917eb5ba8' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\menus\\lists.html',
      1 => 1505655018,
      2 => 'file',
    ),
    'a5f265850ccf14c6541f2361ba1aabeb721c61f8' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\global.html',
      1 => 1505630746,
      2 => 'file',
    ),
    '3e23aa7176b2a134f979e5848a5c3e3676e7d79b' => 
    array (
      0 => '3e23aa7176b2a134f979e5848a5c3e3676e7d79b',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '338459be78eccb6bb7_39116734',
  'variables' => 
  array (
    'css' => 0,
    'each' => 0,
    'absroot' => 0,
    'js' => 0,
    'static' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59be78ece13245_52354126',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59be78ece13245_52354126')) {
function content_59be78ece13245_52354126 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '338459be78eccb6bb7_39116734';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title></title>
    <link rel = "Shortcut Icon" href=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=0">

    
    <?php if ($_smarty_tpl->tpl_vars['css']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['css']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php } else { ?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['js']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php } else { ?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>
   
</head>
 
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('run/Index/index');?>
">后台管理系统V1.0</a>

        </div>
		
        <div class="header-right">

            
            <a href="<?php echo url('home/Index/index');?>
" class="btn btn-primary" title="访问前台">访问前台<i class="glyphicon glyphicon-home"></i></a>
            <a href="login.html" class="btn btn-danger" title="退出登陆">退出<i class="glyphicon glyphicon-log-in"></i></a>

        </div>
    </nav>
    <nav class="navbar-default navbar-side" role="navigation">

        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
				<li>
				    <div class="user-img-div">
				        <img src="<?php echo $_smarty_tpl->tpl_vars['static']->value;?>
images/img29.jpg" class="img-thumbnail" />

				        <div class="inner-text">
				            超级管理员
				        <br />
				            <small>登陆次数 : 2  </small>
				            <small>上次登陆 : 2017-9-16 </small>
				        </div>
				    </div>

				</li>
                <li>
                    <a class="active-menu" href="<?php echo url('run/Menus/lists');?>
"><i class="fa fa-dashboard "></i>导航 / 模块管理</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-desktop "></i>UI Elements <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="panel-tabs.html"><i class="fa fa-toggle-on"></i>Tabs & Panels</a>
                        </li>
                        <li>
                            <a href="notification.html"><i class="fa fa-bell "></i>Notifications</a>
                        </li>
                         <li>
                            <a href="progress.html"><i class="fa fa-circle-o "></i>Progressbars</a>
                        </li>
                         <li>
                            <a href="buttons.html"><i class="fa fa-code "></i>Buttons</a>
                        </li>
                         <li>
                            <a href="icons.html"><i class="fa fa-bug "></i>Icons</a>
                        </li>
                         <li>
                            <a href="wizard.html"><i class="fa fa-bug "></i>Wizard</a>
                        </li>
                         <li>
                            <a href="typography.html"><i class="fa fa-edit "></i>Typography</a>
                        </li>
                         <li>
                            <a href="grid.html"><i class="fa fa-eyedropper "></i>Grid</a>
                        </li>
                        
                       
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-yelp "></i>Extra Pages <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="invoice.html"><i class="fa fa-coffee"></i>Invoice</a>
                        </li>
                        <li>
                            <a href="pricing.html"><i class="fa fa-flash "></i>Pricing</a>
                        </li>
                         <li>
                            <a href="component.html"><i class="fa fa-key "></i>Components</a>
                        </li>
                         <li>
                            <a href="social.html"><i class="fa fa-send "></i>Social</a>
                        </li>
                        
                         <li>
                            <a href="message-task.html"><i class="fa fa-recycle "></i>Messages & Tasks</a>
                        </li>
                        
                       
                    </ul>
                </li>
                <li>
                    <a href="table.html"><i class="fa fa-flash "></i>Data Tables </a>
                    
                </li>
                 <li>
                    <a href="#"><i class="fa fa-bicycle "></i>Forms <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                       
                         <li>
                            <a href="form.html"><i class="fa fa-desktop "></i>Basic </a>
                        </li>
                         <li>
                            <a href="form-advance.html"><i class="fa fa-code "></i>Advance</a>
                        </li>
                         
                       
                    </ul>
                </li>
                  <li>
                    <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>
                </li>
                 <li>
                    <a href="error.html"><i class="fa fa-bug "></i>Error Page</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-sign-in "></i>Login Page</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap "></i>Multilevel Link <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="#"><i class="fa fa-bicycle "></i>Second Level Link</a>
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-flask "></i>Second Level Link</a>
                        </li>
                        <li>
                            <a href="#">Second Level Link<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="#"><i class="fa fa-plus "></i>Third Level Link</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-comments-o "></i>Third Level Link</a>
                                </li>

                            </ul>

                        </li>
                    </ul>
                </li>
               
                <li>
                    <a href="blank.html"><i class="fa fa-square-o "></i>Blank Page</a>
                </li>
            </ul>

        </div>

    </nav>
<div>
 <style type="text/css">
	
.breadcrumb > li + li:before {
   color: green;
  content: "/ ";
   padding: 0 5px;
}


 </style>

<div id="page-wrapper">

<?php
$_smarty_tpl->properties['nocache_hash'] = '338459be78eccb6bb7_39116734';
?>


<style type="text/css">
    .container-fluid{ padding-right: 0;padding-left: 0; }
    /*.nav > li > a{ padding: 10px 229px; }*/
</style> 
<?php echo pr($_smarty_tpl->tpl_vars['result']->value);?>

<div class="container-fluid">
<div class="row">
 <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
     <div class="panel panel-default">
         <div class="panel-heading">
             模块中心
             <a href="<?php echo url('run/Menus/add');?>
" class="btn btn-info" title=" ">添加一级栏目<i class="glyphicon glyphicon-plus"></i></a>
         </div>
         <div class="panel-body">
             <ul class="nav nav-tabs">
                 <li class="active"><a href="#settings" data-toggle="tab"><i class="fa fa-dashboard " style="font-size: 16px;"></i>RUN_后台</a>
                 </li>
                  <li class=""><a href="#home" data-toggle="tab">
                 <i class="glyphicon glyphicon-home"></i>HOME_前台</a>
                 </li>
             </ul>

             <div class="tab-content">
                 <div class="tab-pane fade " id="home">
                     <h4>Home Tab</h4>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                 </div>
                  
                
                 <div class="tab-pane fade active in" id="settings">
                     <div id="treeview6" class=""></div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 </div>
<div>
 
<?php echo '<script'; ?>
 type="text/javascript">

            $(function() { 

        var defaultData = [
         <?php
$_from = $_smarty_tpl->tpl_vars['result']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['value'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['value']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
$foreach_value_Sav = $_smarty_tpl->tpl_vars['value'];
?>
          { 
           <?php if ($_smarty_tpl->tpl_vars['value']->value['status'] == 1) {?>
            text: '<?php echo $_smarty_tpl->tpl_vars['value']->value['catname'];?>
',
           <?php }?>
            href: '#parent1',
            collapseIcon: "glyphicon glyphicon-unchecked",
            tags: ['5'],
/*            nodes: [
              { 
                text: 'Child 1',
                href: '#child1',
                tags: ['2'],
                nodes: [
                  { 
                    text: 'Grandchild 1',
                    href: '#grandchild1',
                    tags: ['0']
                  },
                  { 
                    text: 'Grandchild 2',
                    href: '#grandchild2',
                    tags: ['0']
                  }
                ]
              },
              { 
                text: 'Child 2',
                href: '#child2',
                tags: ['0']
              }
            ]*/
          },
           <?php
$_smarty_tpl->tpl_vars['value'] = $foreach_value_Sav;
}
?>
         /* { 
            text: 'Parent 2',
            href: '#parent2',
            tags: ['0']
          },
          { 
            text: 'Parent 3',
            href: '#parent3',
             tags: ['0']
          },
          { 
            text: 'Parent 4',
            href: '#parent4',
            tags: ['0']
          },
          { 
            text: 'Parent 5',
            href: '#parent5'  ,
            tags: ['0']
          }*/
        ];

 

        var json = '[' +

          '{ ' +
            '"text": "Parent 1",' +
            '"nodes": [' +
              '{ ' +
                '"text": "Child 1",' +
                '"nodes": [' +
                  '{ ' +
                    '"text": "Grandchild 1"' +
                  '},' +
                  '{ ' +
                    '"text": "Grandchild 2"' +
                  '}' +
                ']' +
              '},' +
              '{ ' +
                '"text": "Child 2"' +
              '}' +
            ']' +
          '},' +
          '{ ' +
            '"text": "Parent 2"' +
          '},' +
          '{ ' +
            '"text": "Parent 3"' +
          '},' +
          '{ ' +
            '"text": "Parent 4"' +
          '},' +
          '{ ' +
            '"text": "Parent 5"' +
          '}' +
        ']';


        $('#treeview6').treeview({ 
          color: "green",
          expandIcon: "glyphicon glyphicon-stop",
          collapseIcon: "glyphicon glyphicon-unchecked",
          nodeIcon: "glyphicon glyphicon-user",
          showTags: true,
          data: defaultData
        });
 
        });
<?php echo '</script'; ?>
>

</div>
</body>
</html><?php }
}
?>