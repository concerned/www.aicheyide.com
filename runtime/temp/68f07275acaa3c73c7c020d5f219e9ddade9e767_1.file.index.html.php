<?php /* Smarty version 3.1.27, created on 2017-09-17 14:45:56
         compiled from "E:\phpstudy\WWW\office\application\run\view\index\index.html" */ ?>
<?php
/*%%SmartyHeaderCode:1049859be1a24ba2373_78031713%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '68f07275acaa3c73c7c020d5f219e9ddade9e767' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\index\\index.html',
      1 => 1505555718,
      2 => 'file',
    ),
    'a5f265850ccf14c6541f2361ba1aabeb721c61f8' => 
    array (
      0 => 'E:\\phpstudy\\WWW\\office\\application\\run\\view\\global.html',
      1 => 1505630746,
      2 => 'file',
    ),
    '3720da206fb8c355f29ebc5594fcad2916cbe57c' => 
    array (
      0 => '3720da206fb8c355f29ebc5594fcad2916cbe57c',
      1 => 0,
      2 => 'string',
    ),
  ),
  'nocache_hash' => '1049859be1a24ba2373_78031713',
  'variables' => 
  array (
    'css' => 0,
    'each' => 0,
    'absroot' => 0,
    'js' => 0,
    'static' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_59be1a24cca649_90130695',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_59be1a24cca649_90130695')) {
function content_59be1a24cca649_90130695 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '1049859be1a24ba2373_78031713';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
	<meta http-equiv="Cache-Control" content="no-siteapp" />
	<title></title>
    <link rel = "Shortcut Icon" href=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=0">

    
    <?php if ($_smarty_tpl->tpl_vars['css']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['css']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php } else { ?>
    <link  type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"/>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>

    <?php if ($_smarty_tpl->tpl_vars['js']->value) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['js']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['each'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['each']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['each']->value) {
$_smarty_tpl->tpl_vars['each']->_loop = true;
$foreach_each_Sav = $_smarty_tpl->tpl_vars['each'];
?>
    <?php if (substr($_smarty_tpl->tpl_vars['each']->value,0,7) != 'http://') {?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['absroot']->value;
echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php } else { ?>
    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['each']->value;?>
"><?php echo '</script'; ?>
>
    <?php }?>
    <?php
$_smarty_tpl->tpl_vars['each'] = $foreach_each_Sav;
}
?>
    <?php }?>
   
</head>
 
<body>
<div id="wrapper">
    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo url('run/Index/index');?>
">后台管理系统V1.0</a>

        </div>
		
        <div class="header-right">

            
            <a href="<?php echo url('home/Index/index');?>
" class="btn btn-primary" title="访问前台">访问前台<i class="glyphicon glyphicon-home"></i></a>
            <a href="login.html" class="btn btn-danger" title="退出登陆">退出<i class="glyphicon glyphicon-log-in"></i></a>

        </div>
    </nav>
    <nav class="navbar-default navbar-side" role="navigation">

        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
				<li>
				    <div class="user-img-div">
				        <img src="<?php echo $_smarty_tpl->tpl_vars['static']->value;?>
images/img29.jpg" class="img-thumbnail" />

				        <div class="inner-text">
				            超级管理员
				        <br />
				            <small>登陆次数 : 2  </small>
				            <small>上次登陆 : 2017-9-16 </small>
				        </div>
				    </div>

				</li>
                <li>
                    <a class="active-menu" href="<?php echo url('run/Menus/lists');?>
"><i class="fa fa-dashboard "></i>导航 / 模块管理</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-desktop "></i>UI Elements <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="panel-tabs.html"><i class="fa fa-toggle-on"></i>Tabs & Panels</a>
                        </li>
                        <li>
                            <a href="notification.html"><i class="fa fa-bell "></i>Notifications</a>
                        </li>
                         <li>
                            <a href="progress.html"><i class="fa fa-circle-o "></i>Progressbars</a>
                        </li>
                         <li>
                            <a href="buttons.html"><i class="fa fa-code "></i>Buttons</a>
                        </li>
                         <li>
                            <a href="icons.html"><i class="fa fa-bug "></i>Icons</a>
                        </li>
                         <li>
                            <a href="wizard.html"><i class="fa fa-bug "></i>Wizard</a>
                        </li>
                         <li>
                            <a href="typography.html"><i class="fa fa-edit "></i>Typography</a>
                        </li>
                         <li>
                            <a href="grid.html"><i class="fa fa-eyedropper "></i>Grid</a>
                        </li>
                        
                       
                    </ul>
                </li>
                 <li>
                    <a href="#"><i class="fa fa-yelp "></i>Extra Pages <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="invoice.html"><i class="fa fa-coffee"></i>Invoice</a>
                        </li>
                        <li>
                            <a href="pricing.html"><i class="fa fa-flash "></i>Pricing</a>
                        </li>
                         <li>
                            <a href="component.html"><i class="fa fa-key "></i>Components</a>
                        </li>
                         <li>
                            <a href="social.html"><i class="fa fa-send "></i>Social</a>
                        </li>
                        
                         <li>
                            <a href="message-task.html"><i class="fa fa-recycle "></i>Messages & Tasks</a>
                        </li>
                        
                       
                    </ul>
                </li>
                <li>
                    <a href="table.html"><i class="fa fa-flash "></i>Data Tables </a>
                    
                </li>
                 <li>
                    <a href="#"><i class="fa fa-bicycle "></i>Forms <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                       
                         <li>
                            <a href="form.html"><i class="fa fa-desktop "></i>Basic </a>
                        </li>
                         <li>
                            <a href="form-advance.html"><i class="fa fa-code "></i>Advance</a>
                        </li>
                         
                       
                    </ul>
                </li>
                  <li>
                    <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>
                </li>
                 <li>
                    <a href="error.html"><i class="fa fa-bug "></i>Error Page</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-sign-in "></i>Login Page</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-sitemap "></i>Multilevel Link <span class="fa arrow"></span></a>
                     <ul class="nav nav-second-level">
                        <li>
                            <a href="#"><i class="fa fa-bicycle "></i>Second Level Link</a>
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-flask "></i>Second Level Link</a>
                        </li>
                        <li>
                            <a href="#">Second Level Link<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="#"><i class="fa fa-plus "></i>Third Level Link</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-comments-o "></i>Third Level Link</a>
                                </li>

                            </ul>

                        </li>
                    </ul>
                </li>
               
                <li>
                    <a href="blank.html"><i class="fa fa-square-o "></i>Blank Page</a>
                </li>
            </ul>

        </div>

    </nav>
<div>
 <style type="text/css">
	
.breadcrumb > li + li:before {
   color: green;
  content: "/ ";
   padding: 0 5px;
}


 </style>

<div id="page-wrapper">

<?php
$_smarty_tpl->properties['nocache_hash'] = '1049859be1a24ba2373_78031713';
?>

<ol class="breadcrumb" style="background: #ddd;">
  <li><a href="#">首页</a></li>
  <li><a href="#">我的书</a></li>
  <li class="active">《图解CSS3》</li>
</ol>
 
	 fasdfasfd算法撒旦发生手动阀啊啊啊啊啊撒啊发生的旦发射点飞洒地方手动阀萨弗迪啊师傅

</div>
</body>
</html><?php }
}
?>